"""Function to create grid and launch a batch HERA run over a table of ephemerides"""

# import packages
import sys
import os
import shutil

def genBatchPropConfig():
    """Function to create configuration files for the THALASSA batch propagator"""

    # Import settings
    import heraEphemSettings as cfg

    def createBatchConfig(propPath, gridPath, objName):
        """Function to create THALASSA json settings dictionary"""

        # create settings dictionary
        settings = {}
    
        # output paths
        settings["outputPath"] = {
            "propPath": propPath
        }

        # physical model
        settings["physicalModel"] = {
            "insgrav": cfg.insgrav,
            "isun": cfg.isun,
            "imoon": cfg.imoon,
            "idrag": cfg.idrag,
            "iF107": cfg.iF107,
            "iSRP": cfg.iSRP,
            "iephem": cfg.iephem,
            "gdeg": cfg.gdeg,
            "gord": cfg.gord
        }

        # integration
        settings["integration"] = {
            "tol": cfg.tol,
            "tspan": cfg.tspan,
            "tstep": cfg.tstep,
            "mxstep": cfg.mxstep
        }

        # equations of motion
        settings["eOfM"] = {
            "eqs": cfg.eqs
        }

        # grid
        settings["grid"] = {
            "gridPath": gridPath,
        }

        # output
        settings["output"] = {
            "verb": cfg.verb,
            "objName": objName,
        }

        return settings

    def printConfig(settings, path):
        """Function to print settings dictionary to json file"""

        # Import packages
        import json

        # open file and print
        with open(path, "w") as fp:
            json.dump(settings, fp, indent = 2)

    # create settings dictionaries
    primarySettings   = createBatchConfig('{}/propagate/primaries/orbels'.format(cfg.outputDir), '{}/propagate/primaries/grid'.format(cfg.outputDir), 'primaryEphem')
    secondarySettings = createBatchConfig('{}/propagate/secondaries/orbels'.format(cfg.outputDir), '{}/propagate/secondaries/grid'.format(cfg.outputDir), 'secondaryEphem')

    # print settings dictionaries
    printConfig(primarySettings, '{}/propagate/primaries/settings.json'.format(cfg.outputDir))
    printConfig(secondarySettings, '{}/propagate/secondaries/settings.json'.format(cfg.outputDir))  

def genPostConfig(path):
    """Function to generate JSON configuration file for hera postprocessing"""

    # import packages
    import json

    # import settings
    from heraEphemSettings import explicitIntersectDistance, thalassaPath
    from heraEphemSettings import primaryMass, primaryAreaDrag, primaryAreaSrp, primaryCd, primaryCr
    from heraEphemSettings import secondaryMass, secondaryAreaDrag, secondaryAreaSrp, secondaryCd, secondaryCr
    from heraEphemSettings import postInsgrav, postIsun, postImoon, postIdrag, postIF107, postISRP, postIephem, postGdeg, postGord
    from heraEphemSettings import postTol, postTspan, postTstep, postMxstep, postEqs, postVerb

    # create settings dictionary

    # create path to file
    filePath = "{}/postConfig.json".format(path)

    postSettings = {}

    postSettings["general"] = {
        "explicitIntersectDistance": explicitIntersectDistance,
        "thalassaDir": thalassaPath
    }

    postSettings["physicalChar"] = {
        "primaryMass": primaryMass,
        "primaryAreaDrag": primaryAreaDrag,
        "primaryAreaSrp": primaryAreaSrp,
        "primaryCd": primaryCd,
        "primaryCr": primaryCr,
        "secondaryMass": secondaryMass,
        "secondaryAreaDrag": secondaryAreaDrag,
        "secondaryAreaSrp": secondaryAreaSrp,
        "secondaryCd": secondaryCd,
        "secondaryCr": secondaryCr
    }

    postSettings["physicalModel"] = {
        "insgrav": postInsgrav, 
        "isun": postIsun, 
        "imoon": postImoon, 
        "idrag": postIdrag, 
        "iF107": postIF107, 
        "iSRP": postISRP, 
        "iephem": postIephem, 
        "gdeg": postGdeg, 
        "gord": postGord
    }

    postSettings["integration"] = {
        "tol": "%1.15E" % postTol, 
        "tspan": "%1.15E" % postTspan, 
        "tstep": "%1.15E" % postTstep, 
        "mxstep": "%1.1E" % postMxstep
    }

    postSettings["eOfM"] = {
        "eqs": postEqs
    }

    postSettings["output"] = {
        "verb": postVerb
    }

    # open file and print
    with open(filePath, "w") as fp:
        json.dump(postSettings, fp, indent = 2)

    return filePath   

def makeDir(directoryPath):
    """Function to delete and recreate directories, "clean", directories"""

    # check if directory exists
    if os.path.isdir(directoryPath) is True:
        # delete directory
        shutil.rmtree(directoryPath)
        # replace directory
        os.makedirs(directoryPath)
    else:
        # create directory
        os.makedirs(directoryPath)	
    return

def formatOutputDir(outputDir):
    """Function to format output directory string"""

    if outputDir.endswith('/'):
        return outputDir[:-1]
    else:
        return outputDir
        
def createOutputDir(outputDir):
    """ Function to create output directory structure"""

    # format output directory pth
    outputDir = formatOutputDir(outputDir)
    
    # make directory structure
    makeDir(outputDir)
    os.makedirs('{}/propagate'.format(outputDir))
    os.makedirs('{}/prefilter'.format(outputDir))
    os.makedirs('{}/process'.format(outputDir))
    os.makedirs('{}/propagate/primaries'.format(outputDir))
    os.makedirs('{}/propagate/secondaries'.format(outputDir))
    os.makedirs('{}/propagate/primaries/grid'.format(outputDir))
    os.makedirs('{}/propagate/secondaries/grid'.format(outputDir))
    os.makedirs('{}/propagate/primaries/orbels'.format(outputDir))
    os.makedirs('{}/propagate/secondaries/orbels'.format(outputDir))
    os.makedirs('{}/prefilter/tableOfEphems'.format(outputDir))
    os.makedirs('{}/prefilter/output'.format(outputDir))
    os.makedirs('{}/process/output'.format(outputDir))
    return

# define main function
def main():
    """Main algorithm to be mapped to command line"""

    ### Import packages
    from ephemFunctions.createEphem import createEphem
    from ephemFunctions.launchEphem import launchEphem
    from ephemFunctions.batchProp import thalassaBatchProp
    from postProcessFunctions.processHeraOutput import postProcess
    ## Import settings
    from heraEphemSettings import prop, outputDir, thalassaPath
    from heraEphemSettings import primaryObjectsPath, secondaryObjectsPath
    from heraEphemSettings import postNumCores

    ## Outline algorithm

    # create output directory tree
    createOutputDir(outputDir)

    if prop is True:

        print("THALASSA propagation required...")

        # create batch thalassa propagation configuration inputs
        print("Generating batch configuration files...")
        genBatchPropConfig()

        # propagate primary objects
        print("Propagating primary objects...")
        thalassaBatchProp(thalassaDir=thalassaPath, configPath='{}/propagate/primaries/settings.json'.format(outputDir), objectsPath=primaryObjectsPath, start = 1)

        # propagate secondary objects
        print("Propagating secondary objects...")
        thalassaBatchProp(thalassaDir=thalassaPath, configPath='{}/propagate/secondaries/settings.json'.format(outputDir), objectsPath=secondaryObjectsPath, start = 1)

        # create ephemeris
        print("Creating table of ephemerides for HERA...")
        createEphem()

        # launch ephemeris
        print("Launching HERA over table of ephemerides...")
        launchEphem()

        # create post processing configuration file
        print("Generating configuration files for post processing...")
        configFile = genPostConfig('{}/process'.format(outputDir))

        # post process results
        print("Processing HERA outputs...")
        postProcess(settingsPath=configFile, ephemDirec='{}/prefilter/output'.format(outputDir), outputDirectory='{}/process/output'.format(outputDir), numCores = postNumCores)

    elif prop is False:

        # create ephemeris
        print("Creating table of ephemerides for HERA...")
        createEphem()

        # launch ephemeris
        print("Launching HERA over table of ephemerides...")
        launchEphem()

        # create post processing configuration file
        print("Generating configuration files for post processing...")
        configFile = genPostConfig('{}/process'.format(outputDir))

        # post process results
        print("Processing HERA outputs...")
        postProcess(settingsPath=configFile, ephemDirec='{}/prefilter/output'.format(outputDir), outputDirectory='{}/process/output'.format(outputDir), numCores = postNumCores)

    return

# map command line arguments to function arguments
if __name__ == '__main__':

	main(*sys.argv[1:])
