"""Settings module for the HERA batch mode for a table of ephemerides for the primary and secondary objects"""

### ===================================================== Propagation ===================================================== ###
  
# Should HERA use THALASSA to propagate initial lists of primary and secondary objects?
# prop = True if the objects have not already been propgate
# prop = False the have already been propagated
prop = True

# Specify Output Directory
outputDir = '/Users/nreiland/Documents/oneWebNomHeraOut'

# path to directory where the THALASSA executable is located
thalassaPath = '/Users/nreiland/Documents/codes/thalassa'


#################### If prop = True (objects must first be propagated with THALASSA) ####################

### complete path to the list of PRIMARY objects TO BE PROPAGATED
primaryObjectsPath = '/Users/nreiland/Documents/codes/thalassaSub/in/onewebtarget.txt'

### complete path to the list of SECONDARY objects TO BE PROPAGATED
secondaryObjectsPath = '/Users/nreiland/Documents/codes/thalassaSub/in/onewebfield.txt'

### Thalassa Settings for primary and secondary objects

## PHYSICAL MODEL
# insgrav: 0 = sph. grav. field, 1 = non-spherical grav. field.
# isun:   0 = no Sun perturbation, 1 = otherwise.
# imoon:  0 = no Moon perturbation, 1 = otherwise.
# idrag:  0 = no atmospheric drag, 1 = Wertz model, 2 = US76 (PATRIUS), 3 = J77 (Carrara - INPE), 4 = NRLMSISE-00 (Picone - NRL)
# iF107:  0 = constant F10.7 flux, 1 = variable F10.7 flux
# iSRP:   0 = no SRP perturbation, 1 = SRP, no eclipses, 2 = SRP with conical Earth shadow
# iephem: Ephemerides source. 1 = DE431 ephemerides. 2 = Simpl. Meeus & Brown
# gdeg:   Maximum degree of the gravitational potential.
# gord:   Maximum order of the gravitational potential.
insgrav = 1
isun    = 1
imoon   = 1
idrag   = 4
iF107   = 1
iSRP    = 2
iephem  = 2
gdeg    = 7
gord    = 7

## INTEGRATION
# tolref: Absolute = relative tolerance for the reference propagation
# tol:    Absolute = relative tolerance for the test propagation
# tspan:  Propagation time span (solar days).
# tstep:  Step size (solar days).
# mxstep: Maximum number of integration/output steps.
tol    = 1.000000000000000E-12
tspan  = 90
tstep  = 0.1
mxstep = 1.0E+08

## EQUATIONS OF MOTION
# eqs: Type of the equations of motion.
# 1 = Cowell, 2 = EDromo(t), 3 = EDromo(c), 4 = EDromo(l),
# 5 = KS t), 6 = KS (l), 7 = Sti-Sche (t), 8 = Sti-Sche (l)
eqs = 4

## OUTPUT SETTINGS
# verb:      		1 = Toggle verbose output, 0 = otherwise
# primaryObjName:   Name of THALASSA output files of primary object (saved to primaryPropPaath)
# secondaryObjName: Name of THALASSA output files of secondary object (saved to secondaryPropPath)
verb = 0

#################### If prop = False (objects have alredy been propagated with THALASSA) ####################

### Path to directory where the propagated orbital elements of the *primary* object list are stored
primaryObjsPath = '/Users/nreiland/Documents/codes/thalassaSub/in/onewebtarget.txt'

### Path to directory where the propagated orbital elements of the *secondary* object list are stored
secondaryObjsPath = '/Users/nreiland/Documents/codes/thalassaSub/onewebfield.txt'

### ===================================================== Table of Ephemerides ===================================================== ###

# Specify the step size between THALASSA outputs, which should be used in the generation of the table of ephemerides.
# For example if the THALASSA tstep is 1, a step size of step = 1 will correspond to using every single solar day 
# over the course of the propagation to generate the table of ephemerides (a step size of step = 10 will use every
# 10th day to generate the table of ephemerides).
step = 1

# Specify where in the list of THALASSA outputs the generation of the table of ephemerides should begin. For example
# if the THALASSA tspan is 365 with tstep = 1, an initial entry of initialEntry = 1 will set the beginning of the table 
# of ephemerides at the first day of propagation (if initialEntry = 300 the table of ephemerides will begin on the
# 300th day of propagation).
initialEntry = 1

# Specifiy where in the list of THALASSA outputs the generation of the table of ephemerides should stop. For example
# if the THALASSA tspan is 365 with tstep = 1, a final entry of finalEntry = 365 will set the end of the table of
# ephemerides to the last (365th) day of propagation (if finalEntry = 300, the table of ephemerides will end on the
# 300th day of propagation).
# *** If finalEntry = "all", the generation of the table of ephemerides will end at the final day (or point) of propagation ***
finalEntry = 'all'

### ===================================================== Launch HERA ===================================================== ###

# Specify the intersect distance to be used in the hera prefilters (km)
prefilterIntersectDist = 10

# Specify the number of days over wich HERA should generate time windows for each object (should generally be the same as step)
twinDays = 1

### ===================================================== Postprocess HERA ===================================================== ###

# Specify the intersect distance to be used in the numerical propagation (explicit) filter (km)
explicitIntersectDistance = 1

# specify number of cores to be utilized in post processing ("all" = use all cores)
postNumCores = 'all'

## Specify THALASSA settings for post processing

# PHYSICAL CHARACTERISTICS
# primaryMass: primary object mass (kg)
# primaryAreaDrag: effective drag area of primary object (m^2)
# primaryAreaSrp: effective solar radiation pressure area of primary object (m^2)
# primaryCd: drag coefficient of primary object
# primaryCr: solar radiation pressure coefficient of primary object
# secondaryMass: secondary object mass (kg)
# secondaryAreaDrag: effective drag area of secondary object (m^2)
# secondaryAreaSrp: effective solar radiation pressure area of secondary object (m^2)
# secondaryCd: drag coefficient of secondary object
# secondaryCr: solar radiation pressure coefficient of secondary object
primaryMass = 150
primaryAreaDrag = 1.84
primaryAreaSrp = 1.84
primaryCd = 1.28
primaryCr = 1
secondaryMass = 150
secondaryAreaDrag = 1.84
secondaryAreaSrp = 1.84
secondaryCd = 1.28
secondaryCr = 1

# PHYSICAL MODEL
postInsgrav = 1
postIsun = 1
postImoon = 1
postIdrag = 4
postIF107 = 1
postISRP = 2
postIephem = 2
postGdeg = 7
postGord = 7

# INTEGRATION
# postTol: THALASSA integration tolerance
postTol = 1E-12
# postTspan: the propagation time span in post processing specifies the time window over which THALASSA will numerically
# propagate the orbits of the primary and secondary objects in order to determine the minimum close approach
# distance as well as the time of closest approach. *** In general this value should be the same as the difference
# in time between ephemerides and twindays (the number of days over which HERA generates time windows).
postTspan = 0.1
# postTstep: this value specifies the time step between THALASSA outputs i.e. how often the relative distance
# between primary and secondary objects will be checked.
postTstep = 0.000001157407/2
# postMxstep: THALASSA maximum number of intergration/output steps
postMxstep = 1.0E+08

# EQUATIONS OF MOTION
postEqs = 4

# OUTPUT SETTINGS
postVerb = 0
