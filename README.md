**HERA close approach prediction code**

HERA is a python code close approach prediction code, which utilizes the analytical prefilters outlined in Hoot's et. al. 1984 as well
as numerical orbit propagation.

---

## Geometrical Prefilters

In order to reduce the number of numerical orbit propagations performed, the following two geometrical prefilters are utilized.

1. The apogee-perigee test.
2. Calculation of the two points of closest approach along the line of intersection between the primary and secondary objects.

---

## Time prefilter

The objects which pass through the first two geometrical prefilters are then subjected to the time prefilter. The purpose of the time prefilter is to generate sets of time windows for both primary and secondary objects, when they pass through the line of intersection and a briefly vulnerable.

---

## Numerical progation

The objects, which pass throught the first three prefilters are then propagated over a specified timespan and the time of closest approach and minimum separation distance are computed.