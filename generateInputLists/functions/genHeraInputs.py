"""Script to create batch input list .txt file for HERA close approch prediction python code"""

def genHeraList(inputFile, outputDir, listName):
	"""Function to generate a list of input objects for HERA close approach prediction"""

	# Import Packages
	import pandas as pd

	# Define DataFrames
	dataframes = []

	# Load spreadsheet
	xl = pd.ExcelFile(inputFile)

	# Parse sheets
	sheets = xl.sheet_names

	# Load sheets into dataframes
	for i, dummy in enumerate(sheets):

		# Load sheet into dataframe 
		df = xl.parse(sheets[i])
		dataframes.append(df)

	for j, dummy in enumerate(sheets):

		# define file name
		sheet_out = sheets[j]
		fname = "{}/{}_{}.txt".format(outputDir, listName, sheet_out)
	
		# define data for loop
		data = dataframes[j]
		columns = data.columns

		# define headers
		headers = []
		for i, dummy in enumerate(columns):
			header = columns[i]
			headers.append(header)

		# sort data into lists based on headers
		data_lists = []
		for line in headers:
			list_header = data[line]
			data_lists.append(list_header) 

		# define lists of  orbital elements
		MJD_list       = data_lists[0]
		SMA_list       = data_lists[1]
		ECC_list       = data_lists[2]
		INC_list       = data_lists[3]
		RAAN_list      = data_lists[4]
		AOP_list       = data_lists[5]
		M_list         = data_lists[6]

		# open file
		with open(fname, 'w') as fp:

			# write header
			fp.write("MJD                    SMA                    ECC                    INC                    RAAN                   AOP                    M                    \n")
			fp.write(" \n")

			# loop through elements and write to file
			for i, dummy in enumerate(SMA_list):

				# define strings
				MJD_str       = str(MJD_list[i])
				SMA_str       = str(SMA_list[i])
				ECC_str       = str(ECC_list[i])
				INC_str       = str(INC_list[i])
				RAAN_str      = str(RAAN_list[i])
				AOP_str       = str(AOP_list[i])
				M_str         = str(M_list[i])
				
				# convert to floats
				MJD_val       = float(MJD_str)
				SMA_val       = float(SMA_str)
				ECC_val       = float(ECC_str)
				INC_val       = float(INC_str)
				RAAN_val      = float(RAAN_str)
				AOP_val       = float(AOP_str)
				M_val         = float(M_str)
				
				# format data entries
				MJD       = "%1.15E" % MJD_val
				SMA       = "%1.15E" % SMA_val
				ECC       = "%1.15E" % ECC_val
				INC       = "%1.15E" % INC_val
				RAAN      = "%1.15E" % RAAN_val
				AOP       = "%1.15E" % AOP_val
				M         = "%1.15E" % M_val

				# write to file
				fp.write("{}, {}, {}, {}, {}, {}, {}\n".format(MJD, SMA, ECC, INC, RAAN, AOP, M))

		# print output
		print("Orbels for HERA successfully saved to: {}".format(fname))	

	return
	