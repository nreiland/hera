"""Settings module for the generation of HERA or THALASSA input lists"""

# specify if a batch THALASSA propagation input list should be generated
# ***True*** to create a list of THALASSA inputs and ***False*** if no list should be created
genThalassaInput = True

# specify if a batch HERA input list should be generated
# ***True*** to create a list of HERA inputs and ***False*** if no list should be created
genHeraInput = True

################################# Settings for generating ***THALASSA** inputs #################################

# specify complete path to input excel file containing object data
thalassaInputFile = "/Users/User/Documents/CODES/hera/generateInputLists/exampleObjects.xlsx"

# specify output directory where the .txt read for THALASSA batch propagation should be saved
thalassOuputDir = "/Users/User/Documents/CODES/hera/data/ThalassaBatchIn"

# speficy the desired name of the output .txt list
thalassaListName = "exampleThalassaList"

################################### Settings for generating ***HERA** inputs ###################################

# specify complete path to input excel file containing object data
heraInputFile = "/Users/User/Documents/CODES/hera/generateInputLists/exampleObjects.xlsx"

# specify output directory where the .txt read for THALASSA batch propagation should be saved
heraOuputDir = "/Users/User/Documents/CODES/hera/data/heraBatchIn"

# speficy the desired name of the output .txt list
heraListName = "exampleHeraList"
