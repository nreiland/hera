"""Function to be run from the command line for the generation of THALASSA and HERA input lists"""

# import functions
from functions.genThalassaInputs import genThalassaList
from functions.genHeraInputs import genHeraList

# import initilization settings
from inputListSettings import genHeraInput, genThalassaInput

if genHeraInput is True:

    # import HERA list generation settings
    from inputListSettings import heraInputFile, heraOuputDir, heraListName

    # generate HERA input list
    genHeraList(heraInputFile, heraOuputDir, heraListName)

if genThalassaInput is True:

    # import THALASSA list generation settings

    from inputListSettings import thalassaInputFile, thalassOuputDir, thalassaListName

    # generate THALASSA input list
    genThalassaList(thalassaInputFile, thalassOuputDir, thalassaListName)
