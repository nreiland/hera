"""Script for launching HERA over an ephemeris grid of THALASSA outputs"""

def launchEphem():
	"""Function to launch HERA over an ephemeris grid of THALASSA outputs"""

	## Import packages
	import os
	import shutil
	import json
	from hera import run_HERA

	def cleanDir(dirPath):
		"""Function to create output directory and delete/recreate if it already exists"""

		# check if directory exists
		if os.path.isdir(dirPath) is True:

			# delete directory
			shutil.rmtree(dirPath)

			# replace directory
			os.makedirs(dirPath)

		else:
			
			# create directory
			os.makedirs(dirPath)	

		return 	

	def create_inputs(ephemerides_direc, output_directory, intersect_distance, days):
		"""Function to create a list of inputs for HERA"""

		# Create a list of ephemeris folder names
		ephem_folder_names = os.listdir(ephemerides_direc)

		# Remove DS.Store from list

		# define indexing
		idx = 0

		for line in ephem_folder_names:

			if ephem_folder_names[idx].endswith(".DS_Store"):

				del ephem_folder_names[idx]

				# step back indexing
				idx = idx - 1

			# update indexing
			idx = idx + 1	

		# Create output paths
		output_folder_paths = []

		for line in ephem_folder_names:

			# define folder path
			out_folder_path = "{}/{}".format(output_directory, line)

			# add path to list
			output_folder_paths.append(out_folder_path)

			# if directory exists delete and recreate else simply create
			cleanDir(out_folder_path)

		# Read HERA settings file		

		# Create settings files

		for line in output_folder_paths:

			# define path
			jsonPath = "{}/settings.json".format(line)

			# define settings dictionaary
			heraSettings = {}

			# paths
			heraSettings["paths"] = {
				"primaryPath": "{}/primaries.txt".format(line),
				"secondaryPath": "{}/secondaries.txt".format(line),
				"outputPath": line
			}

			# analysis
			heraSettings["analysisSettings"] = {
				"intersectDistance": intersect_distance,
				"days": days
			}
			
			# open path and print settings file
			with open(jsonPath, "w") as fp:
				json.dump(heraSettings, fp, indent = 2)

		# transfer input files to output directory

		for line in ephem_folder_names:

			# define paths
			primaries_path   = "{}/{}/primaries.txt".format(ephemerides_direc, line)
			secondaries_path = "{}/{}/secondaries.txt".format(ephemerides_direc, line)

			# define foler to move files to
			path_out = "{}/{}".format(output_directory, line)

			# copy files to ouput directories
			shutil.copy2(primaries_path, path_out)
			shutil.copy2(secondaries_path, path_out)

		return ephem_folder_names	

	def launch_HERA(ephem_folder_names, output_directory):
		"""Function to launch HERA for each ephemeris in the input list"""

		for line in ephem_folder_names:

			# define settings path for HERA
			settings_path = "{}/{}/settings.json".format(output_directory, line)
			
			# run HERA
			print("running case defined by {}".format(settings_path))
			run_HERA(settings_path)

		return	
			
	### Defin Main Algorithm

	# import settings
	from heraEphemSettings import outputDir, prefilterIntersectDist, twinDays
	
	# create a list of ephemerides folder names
	ephem_folder_names = create_inputs('{}/prefilter/tableOfEphems'.format(outputDir), '{}/prefilter/output'.format(outputDir), prefilterIntersectDist, twinDays)

	# launch hera for each ephemeris
	launch_HERA(ephem_folder_names, '{}/prefilter/output'.format(outputDir))

	return	
