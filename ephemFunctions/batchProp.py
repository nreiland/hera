"""Script for launching a batch THALASSA run over a list of objects (see documentation)"""

def thalassaBatchProp(thalassaDir, configPath, objectsPath, start):
	"""Function for launching a batch THALASSA run over a list of objects"""

	### Import packages
	import subprocess
	import os
	import shutil

	def launchThalassa(settings, obj, thalassaDir):
		"""Function to launch the THALASSA executable"""

		# define arguments
		arg1 = "./thalassa.x"
		arg2 = settings
		arg3 = obj
		print("{} {} {}".format(arg1, arg2, arg3))

		# change working directory to thalassa directory if not already there

		# check current working directory
		cwd = os.getcwd()

		if cwd == thalassaDir:

			# run thalassa
			subprocess.call([arg1, arg2, arg3], stdout = None)

		else:

			# change to thalassa direcotry	
			os.chdir(thalassaDir)

			# run thalassa
			subprocess.call([arg1, arg2, arg3], stdout = None)

		return

	def numFormat(val, string):
		"""Function to format floating point numbers for THALASSA"""
		
		if val < 0:

			out = "{}".format(string)

		elif val >= 0:

			out = "+{}".format(string) 	

		return out

	def cleanDirectory(directory_path):
		"""Function to delete and recreate directories, "clean", directories"""

		# check if directory exists
		if os.path.isdir(directory_path) is True:

			# delete directory
			shutil.rmtree(directory_path)

			# replace directory
			os.makedirs(directory_path)

		else:
			
			# create directory
			os.makedirs(directory_path)	

		return

	def createGrid(configPath, objectsList):

		# import packages
		import json

		# Open batch settings file and populate lines
		with open(configPath, "r") as f_obj:
			settings = json.load(f_obj)

		# grab strings
		gridPath  = settings["grid"]["gridPath"]
		outputDir = settings["outputPath"]["propPath"]
		objName   = settings["output"]["objName"]

		# clean output directory
		cleanDirectory(outputDir)

		# create lists for paths
		objectPathList  = []
		settingsPathList = []

		# loop through objects and create grid
		for idx, orbels in enumerate(objectsList, start = 1):

			# define location of input directory
			inputDir = "{}/{}".format(gridPath, idx)

			# create directory for Thalassa input
			cleanDirectory(inputDir)

			# define object path
			objectPath = "{}/object.txt".format(inputDir)

			# define output directory path
			outputPath = "{}/{}{}".format(outputDir, objName, idx)

			# define inputs for writeObject
			MJD       = orbels[0]
			SMA       = orbels[1]
			ECC       = orbels[2]
			INC       = orbels[3]
			RAAN      = orbels[4]
			AOP       = orbels[5]
			M         = orbels[6]
			Mass      = orbels[7]
			Area_drag = orbels[8]
			Area_SRP  = orbels[9]
			CD        = orbels[10]
			CR        = orbels[11]

			# write object
			writeObject(objectPath, MJD, SMA, ECC, INC, RAAN, AOP, M, Mass, Area_drag, Area_SRP, CD, CR)

			# define settings path
			settingsPath = "{}/settings.txt".format(inputDir)

			# write settings
			writeSettings(settingsPath, settings, outputPath)

			# add paths to lists
			objectPathList.append(objectPath)
			settingsPathList.append(settingsPath)

		return {
			"objectPathList": objectPathList,
			"settingsPathList": settingsPathList
		}

	def writeSettings(settingsPath, settings, outputPath):
		"""Function to write gride of settings files for batch THALASSA runs"""
	
		# Physical model	
		insgrav = settings["physicalModel"]["insgrav"]
		isun    = settings["physicalModel"]["isun"]
		imoon   = settings["physicalModel"]["imoon"]
		idrag   = settings["physicalModel"]["idrag"]
		iF107   = settings["physicalModel"]["iF107"]
		iSRP    = settings["physicalModel"]["iSRP"]
		iephem  = settings["physicalModel"]["iephem"]
		gdeg    = settings["physicalModel"]["gdeg"]
		gord    = settings["physicalModel"]["gord"]

		# Integration
		tol    = settings["integration"]["tol"]
		tspan  = settings["integration"]["tspan"]
		tstep  = settings["integration"]["tstep"]
		mxstep = settings["integration"]["mxstep"]

		# Equations of motion
		eqs = settings["eOfM"]["eqs"]

		# Output
		verb     = settings["output"]["verb"]

		# convert  to correctly formatted strings
		tol    = "%1.15E" % tol
		tspan  = "%1.15E" % tspan
		tstep  = "%1.15E" % tstep
		mxstep = "%1.1E" % mxstep

		# Write settings file
		with open(settingsPath, 'w') as fp:
			fp.write("# THALASSA - SETTINGS\n")
			fp.write("# ==============================================================================\n")
			fp.write(" \n")
			fp.write("# PHYSICAL MODEL\n")
			fp.write("# insgrav:   0 = sph. grav. field, 1 = non-spherical grav. field.\n")
			fp.write("# isun:      0 = no Sun perturbation, 1 = otherwise.\n")
			fp.write("# imoon:     0 = no Moon perturbation, 1 = otherwise.\n")
			fp.write("# idrag:     0 = no atmospheric drag, 1 = Wertz model, 2 = US76 (PATRIUS), 3 = J77 (Carrara - INPE), 4 = NRLMSISE-00 (Picone - NRL)\n")
			fp.write("# iF107:     0 = constant F10.7 flux, 1 = variable F10.7 flux\n")
			fp.write("# iSRP:      0 = no SRP perturbation, 1 = SRP, no eclipses, 2 = SRP with conical Earth shadow\n")
			fp.write("# iephem:    Ephemerides source. 1 = DE431 ephemerides. 2 = Simpl. Meeus & Brown\n")
			fp.write("# gdeg:      Maximum degree of the gravitational potential.\n")
			fp.write("# gord:      Maximum order of the gravitational potential.\n")
			fp.write("insgrav:   {}\n".format(insgrav))
			fp.write("isun:      {}\n".format(isun))
			fp.write("imoon:     {}\n".format(imoon))
			fp.write("idrag:     {}\n".format(idrag))
			fp.write("iF107:     {}\n".format(iF107))
			fp.write("iSRP:      {}\n".format(iSRP))
			fp.write("iephem:    {}\n".format(iephem))
			fp.write("gdeg:      {}\n".format(gdeg))
			fp.write("gord:      {}\n".format(gord))
			fp.write(" \n")
			fp.write("# INTEGRATION\n")
			fp.write("# tol:       Absolute = relative tolerance for the test propagation\n")
			fp.write("# tspan:     Propagation time span (solar days).\n")
			fp.write("# tstep:     Step size (solar days).\n")
			fp.write("# mxstep:    Maximum number of integration/output steps.\n")
			fp.write("tol:       {}\n".format(tol))
			fp.write("tspan:     {}\n".format(tspan))
			fp.write("tstep:     {}\n".format(tstep))
			fp.write("mxstep:    {}\n".format(mxstep))
			fp.write(" \n")
			fp.write("# EQUATIONS OF MOTION\n")
			fp.write("# eqs:       Type of the equations of motion.\n")
			fp.write("#            1 = Cowell, 2 = EDromo(t), 3 = EDromo(c), 4 = EDromo(l),\n")
			fp.write("#            5 = KS (t), 6 = KS (l), 7 = Sti-Sche (t), 8 = Sti-Sche (l)\n")
			fp.write("eqs:       {}\n".format(eqs))
			fp.write(" \n")
			fp.write("# OUTPUT SETTINGS\n")
			fp.write("# verb:      1 = Toggle verbose output, 0 = otherwise\n")
			fp.write("# out:       Full path to output directory\n")
			fp.write("verb:      {}\n".format(verb))
			fp.write("out:   {}\n".format(outputPath))

		return

	def loadObjects(objectsPath, start):
		"""Function to load current object file from input list"""

		# define output list
		objectsStr = []
		objects     = []

		# open orbels file
		with open(objectsPath) as f_obj:
			lines = f_obj.readlines()
			lines = lines[start:len(lines)]

		# remove excess from strings
		for line in lines:
			data = line.rstrip()
			objectsStr.append(data)

		for line in objectsStr:

			# define output list
			orbels = []
			
			# split string at commas and create list
			data = [x.strip() for x in line.split(',')]

			# Define Orbels
			orbels.append(data[0])
			orbels.append(data[1])
			orbels.append(data[2])
			orbels.append(data[3])
			orbels.append(data[4])
			orbels.append(data[5])
			orbels.append(data[6])
			orbels.append(data[7])
			orbels.append(data[8])
			orbels.append(data[9])
			orbels.append(data[10])
			orbels.append(data[11])

			# add to object_orbels
			objects.append(orbels)

		return objects

	def writeObject(objectPath, MJD, SMA, ECC, INC, RAAN, AOP, M, Mass, Area_drag, Area_SRP, CD, CR):
		"""Function to write an object files for batch THALASSA run"""
		
		## Convert inputs to correct format

		# Define floats
		MJD_val       = float(MJD)  
		SMA_val       = float(SMA) 
		ECC_val       = float(ECC) 
		INC_val       = float(INC) 
		RAAN_val      = float(RAAN) 
		AOP_val       = float(AOP) 
		M_val         = float(M) 
		Mass_val      = float(Mass) 
		Area_drag_val = float(Area_drag) 
		Area_SRP_val  = float(Area_SRP) 
		CD_val        = float(CD) 
		CR_val        = float(CR)
		
		# add exponential notation
		MJD       = '{:.15E}'.format(MJD_val)
		SMA       = '{:.15E}'.format(SMA_val)
		ECC       = '{:.15E}'.format(ECC_val)
		INC       = '{:.15E}'.format(INC_val)
		RAAN      = '{:.15E}'.format(RAAN_val)
		AOP       = '{:.15E}'.format(AOP_val)
		M         = '{:.15E}'.format(M_val)
		Mass      = '{:.15E}'.format(Mass_val)
		Area_drag = '{:.15E}'.format(Area_drag_val)
		Area_SRP  = '{:.15E}'.format(Area_SRP_val)
		CD        = '{:.15E}'.format(CD_val)
		CR        = '{:.15E}'.format(CR_val)
     
		# Add Signs
		MJD       = numFormat(MJD_val, MJD)
		SMA       = numFormat(SMA_val, SMA)
		ECC       = numFormat(ECC_val, ECC)
		INC       = numFormat(INC_val, INC)
		RAAN      = numFormat(RAAN_val, RAAN)
		AOP       = numFormat(AOP_val, AOP)
		M         = numFormat(M_val, M)
		Mass      = numFormat(Mass_val, Mass)
		Area_drag = numFormat(Area_drag_val, Area_drag)
		Area_SRP  = numFormat(Area_SRP_val, Area_SRP)
		CD        = numFormat(CD_val, CD)
		CR        = numFormat(CR_val, CR)

		## Write Object File
		with open(objectPath, 'w') as fp:
			fp.write("# THALASSA - OBJECT PARAMETERS\n")
			fp.write("# ==============================================================================\n")
			fp.write("# Initial epoch and orbital elements\n")
			fp.write("{}; MJD  [UT1]\n".format(MJD))
			fp.write("{}; SMA  [km]\n".format(SMA))
			fp.write("{}; ECC  [-]\n".format(ECC))
			fp.write("{}; INC  [deg]\n".format(INC))
			fp.write("{}; RAAN [deg]\n".format(RAAN))
			fp.write("{}; AOP  [deg]\n".format(AOP))
			fp.write("{}; M    [deg]\n".format(M))
			fp.write("# Physical characteristics\n")
			fp.write("{}; Mass [kg]\n".format(Mass))
			fp.write("{}; Area (drag) [m^2]\n".format(Area_drag))
			fp.write("{}; Area (SRP)  [m^2]\n".format(Area_SRP))
			fp.write("{}; CD   [-]\n".format(CD))
			fp.write("{}; CR   [-]\n".format(CR))

		return

	def BatchPropIterObject(settingsPathList, objectPathList, thalassaDir):
		"""Function to launch batch propagation across multiple processors"""
		
		# Import Packages
		from joblib import Parallel, delayed
		import multiprocessing

		# Define input
		INPUT = []

		# Populate input
		for i, dummy in enumerate(settingsPathList):
			inputLine = [settingsPathList[i], objectPathList[i]]
			INPUT.append(inputLine)

		# Define functions

		def processInput(INPUT):
			""""Process input" function for multiprocessing"""

			settingsPath = INPUT[0]
			inputPath    = INPUT[1]

			return launchThalassa(settings = settingsPath, obj = inputPath, thalassaDir = thalassaDir)

		# Count number of cores
		numCores = multiprocessing.cpu_count()

		# Run processes
		Parallel(n_jobs = numCores)(delayed(processInput)(i) for i in INPUT)
		
		return numCores

	### Define Algorithm

	# load batch objects file and return list of objects
	objectsList = loadObjects(objectsPath, start)

	# create grid of inputs
	inputs = createGrid(configPath, objectsList)

	## Run Batch Propagation
	BatchPropIterObject(inputs["settingsPathList"], inputs["objectPathList"], thalassaDir)

	return
 
