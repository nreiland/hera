"""Script for creating an ephemeris grid from THALASSA outputs to be used for batch HERA analysis"""

def createEphem():
	"""Funtion for creatingg an ephemeris grid from THALASSA outputs to be used for batch HERA analysis"""

	### Import packages
	import os
	import shutil

	### Define functions

	def create_output_direc(output_directory):
		"""Function to create output directory and delete/recreate if it already exists"""

		if os.path.exists(output_directory):

			shutil.rmtree(output_directory)

			os.makedirs(output_directory)

		else:

			os.makedirs(output_directory)

		return 	

	def read_inputs(primary_obj_ephem_path, secondary_obj_ephem_path):
		"""Function to read inputs form THALASSA output directory"""

		# define additional function
		def graborbels(fNames):
			"""Function to only add orbels.dat files to the list of files to be read"""

			fNameOrbels = []

			for line in fNames:

				if line[-10:-4] == "orbels":

					fNameOrbels.append(line)

			return fNameOrbels

		# Create output directories
		primary_fpaths   = []
		secondary_fpaths = []

		# Create list of primary object names
		primary_fnames = os.listdir(primary_obj_ephem_path)

		# Only grab orbels.dat files
		primary_fnames = graborbels(primary_fnames)
	
		# create list of secondary object names
		secondary_fnames = os.listdir(secondary_obj_ephem_path)

		# Only grab orbels.dat files
		secondary_fnames = graborbels(secondary_fnames)
	
		# create list of primary object paths
		for idx, dummy in enumerate(primary_fnames, start = 0):

			# define file path
			fpath = "{}/{}".format(primary_obj_ephem_path, primary_fnames[idx])

			# add file path to output folder
			primary_fpaths.append(fpath)

		# remove .DS_Store files from primary list
		for idx, dummy in enumerate(primary_fpaths, start = 0):

			if primary_fpaths[idx].endswith(".DS_Store"):

				del primary_fpaths[idx]

		# create list of secondary object paths
		for idx, dummy in enumerate(secondary_fnames, start = 0):

			# define file path
			fpath = "{}/{}".format(secondary_obj_ephem_path, secondary_fnames[idx])

			# add file path to output folder
			secondary_fpaths.append(fpath)

		# remove .DS_Store files from primary list
		for idx, dummy in enumerate(secondary_fpaths, start = 0):

			if secondary_fpaths[idx].endswith(".DS_Store"):

				del secondary_fpaths[idx]

		return [primary_fpaths, secondary_fpaths]

	def sortPaths(paths):
		"""Function to sort THALASSA output paths using batchProp index"""

		# import packages
		import operator

		def numFromString(myString):
			"""Function to grab a number from a string for sorting"""
			
			val = ""
			for char in myString:
				try:
					dummy = int(char)
					val += char
				except ValueError:
					continue

			return int(val)    
		
		# define dictionary
		pathsDict = {}

		for path in paths:
			desig = numFromString(path)
			pathsDict[desig] = path

		sortedPathsDict = sorted(pathsDict.items(), key=operator.itemgetter(0))

		# define sorted list
		sortedPaths = []

		for entry in sortedPathsDict:
			sortedPaths.append(entry[1])

		return sortedPaths	

	def create_orbels_table(primary_fpaths, secondary_fpaths, step = 1, initial_entry = 1, final_entry = "all"):
		"""Function to create a table of orbital element paths for the specifified primary and secondary satellites"""

		# define output lists
		primary_orbels_table   = []
		secondary_orbels_table = []

		# open every primary object and grab orbital elements for each specified time step
		for line in primary_fpaths:

			# define output list
			primary_orbels = []

			# opening file
			with open(line, 'r') as f_obj:

				# read lines
				lines = f_obj.readlines()

			# grab specified number of entries
			if final_entry == "all":

				lines = lines[3:len(lines)]

			else:
				
				lines = lines[2 + initial_entry:final_entry + 3]

			# place entry specified by step into new list

			# define initial index
			idx = 0

			# loop through list
			while idx <= len(lines) - 1:

				# add specified entry into list of orbels for that primary object
				primary_orbels.append(lines[idx])

				# update indexing according to step size
				idx = idx + step

			# add orbels for current primary object into the primary object ephemerides table
			primary_orbels_table.append(primary_orbels)	

		# open every secondary object and grab orbital elements for each specified time step
		for line in secondary_fpaths:

			# define output list
			secondary_orbels = []

			# opening file
			with open(line, 'r') as f_obj:

				# read lines
				lines = f_obj.readlines()

			# grab specified number of entries
			if final_entry == "all":

				lines = lines[3:len(lines)]

			else:
				
				lines = lines[2 + initial_entry:final_entry + 3]

			# place entry specified by step into new list

			# define initial index
			idx = 0

			# loop through list
			while idx <= len(lines) - 1:

				# add specified entry into list of orbels for that secondary object
				secondary_orbels.append(lines[idx])

				# update indexing according to step size
				idx = idx + step

			# add orbels for current secondary object into the primary object ephemerides table
			secondary_orbels_table.append(secondary_orbels)		

		# Define empherides tables
		orbels_table = [primary_orbels_table, secondary_orbels_table]

		return orbels_table

	def create_ephemerides_tables(primary_orbels_table, secondary_orbels_table):
		"""Function to create a table of ephemerides of orbital elements for the specified primary and secondary objects"""

		# determine number of ephemerides tables by sampling a test case
		sample = primary_orbels_table[0]

		# count number of ephemerides
		num_emphemerides = len(sample)

		# create empty list for each ephemerides
		ephemerides = [[] for _ in range(num_emphemerides)]

		# add correct values to ephemerides lists
		for idx, dummy in enumerate(ephemerides, start = 0):

			#  define output lists
			primaries   = []
			secondaries = []

			# loop through primaries and grab orbels for the corresponding ephemeris
			for line in primary_orbels_table:

				# define primary object
				primary_object = line

				# add exception for short list
				try:

					# define orbels corresponding to the indexed ephemeris
					ephem_orbels = primary_object[idx]

				except IndexError:

					continue	

				# add orbels to primary object output list
				primaries.append(ephem_orbels)

			# loop through secondaries and grab orbels for the corresponding ephemeris
			for line in secondary_orbels_table:

				# define primary object
				secondary_object = line

				# add exception for short list
				try:

					# define orbels corresponding to the indexed ephemeris
					ephem_orbels = secondary_object[idx]

				except IndexError:

					continue	

				# add orbels to primary object output list
				secondaries.append(ephem_orbels)

			# create new entry for this ephemeris
			ephemeris_objects = [primaries, secondaries]

			# add entry to correspoding ephemeris list
			ephemerides[idx].append(ephemeris_objects)		

		for line in ephemerides:
			ephem = line
			primaries = ephem[0][0]
			secondaries = ephem[0][1]	

		return ephemerides

	def print_ephemerides_inputs(ephemerides_table, output_directory):
		"""Function to print the ephemerides of the objects to text files in the specified output directory"""

		# Create a new directory for each ephemeris
		
		# define indexing for folder nameing
		idx = 1

		# loop through ephemerides table and create directories
		for line in ephemerides_table:

			# define ephemeris objects entry
			ephemeris_objects = line

			# create directory path
			path = "{}/ephemeris_{}".format(output_directory, idx)

			# if directory exists delete and recreate else simply create
			if not os.path.exists(path):

				os.makedirs(path)

			else:

				shutil.rmtree(path)

				os.makedirs(path)

			# create primary object files

			# define file path
			fpath = "{}/primaries.txt".format(path)

			# open primary file and write objects
			with open(fpath, 'w') as fp:

				# write header
				fp.write("MJD                    SMA                    ECC                    INC                    RAAN                   AOP                    M                    \n")
				fp.write(" \n")

				# loop through primary objects and print orbital elements
				primaries = ephemeris_objects[0][0]

				for entry in primaries:

					fp.write(entry)

			# create secondary object files

			# define file path
			fpath = "{}/secondaries.txt".format(path)

			# open primary file and write objects
			with open(fpath, 'w') as fp:

				# write header
				fp.write("MJD                    SMA                    ECC                    INC                    RAAN                   AOP                    M                    \n")
				fp.write(" \n")

				# loop through primary objects and print orbital elements
				secondaries = ephemeris_objects[0][1]

				for entry in secondaries:

					fp.write(entry)					

			# update index
			idx = idx + 1		
		
		return		

	### MAIN ALGORITHM

	# import settings
	from heraEphemSettings import prop, outputDir, primaryObjsPath, secondaryObjsPath, step, initialEntry, finalEntry

	# parse settings
	output_directory = '{}/prefilter/tableOfEphems'.format(outputDir)
	if prop is True:
		primary_obj_ephem_path   = '{}/propagate/primaries/orbels'.format(outputDir)
		secondary_obj_ephem_path = '{}/propagate/secondaries/orbels'.format(outputDir)
	else:
		primary_obj_ephem_path   = primaryObjsPath
		secondary_obj_ephem_path = secondaryObjsPath

	# create output directory
	create_output_direc(output_directory)	

	# load ephemerides of primary and secondary objects
	output           = read_inputs(primary_obj_ephem_path, secondary_obj_ephem_path)
	primary_fpaths   = output[0]
	secondary_fpaths = output[1]

	# sort paths
	primary_fpaths = sortPaths(paths = primary_fpaths)
	secondary_fpaths = sortPaths(paths = secondary_fpaths)

	# create ephemerides table
	orbels_table            = create_orbels_table(primary_fpaths, secondary_fpaths, step, initialEntry, finalEntry)
	primary_orbels_table    = orbels_table[0]
	secondary_orbels_table  = orbels_table[1]

	# create ephemerides tables
	ephemerides_table = create_ephemerides_tables(primary_orbels_table, secondary_orbels_table)

	print_ephemerides_inputs(ephemerides_table, output_directory)

	return	
