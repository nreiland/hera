"""hera close approach prediction software - Nathan Reiland - University of Arizona - SAMOS"""
##### Import packages
import sys
import os
import shutil
import multiprocessing
from joblib import Parallel, delayed
from mainFunctions.heraFunctions import load_inputs, prefilter1, prefilter2, time_prefilter, create_summary, readJsonSettings
from mainFunctions.investigateCas import investigate_cas, investigateCasPrinterJson

##### Define main function
def run_HERA(settings_path = "mainFunctions/in/settings.json"):
	"""Main function for running hera"""

	### Define functions

	# Function to clean directories
	def clean_directory(directory_path):
		"""Function to delete and recreate directories, "clean", directories"""

		# check if directory exists
		if os.path.isdir(directory_path) is True:

			# delete directory
			shutil.rmtree(directory_path)

			# replace directory
			os.makedirs(directory_path)

		else:
			
			# create directory
			os.makedirs(directory_path)	

		return

	# Define input processing function to run hoot's method
	def processInput(Input):
		"""Function for mulitprocessing hera inputs"""

		## Parse Input
		idx         = Input[0]
		primary     = Input[1]
		secondaries = Input[2]

		## Define Primary Orbital Elements
		jd_p = float(primary[0])
		a_p = float(primary[1])
		e_p = float(primary[2])
		i_p = float(primary[3])
		W_p = float(primary[4])
		w_p = float(primary[5])
		M_p = primary[6]

		## Run Hoot's Algorithm

		# prefilter 1
		output = prefilter1(intersect_distance, a_p, e_p, secondaries)
		new_secondaries_1 = output[0]

		if len(new_secondaries_1) >= 1:

			# prefilter 2
			output = prefilter2(intersect_distance, a_p, e_p, i_p, W_p, w_p, new_secondaries_1)
			new_secondaries_2 = output[0]
			coplanar_secondaries_2 = output[2]

			if len(new_secondaries_2) >= 1:

				# prefilter 3
				timePrefilterOutput   = time_prefilter(intersect_distance, a_p, e_p, i_p, W_p, w_p, new_secondaries_2, days)
				new_secondaries_3      = timePrefilterOutput['newSecondaries']
				overlap_sets           = timePrefilterOutput['overlapSet']
				coplanar_secondaries_3 = timePrefilterOutput['coplanarSecondaries']

				# investigate time windows of close approach secondaries
				#overlap_sets = investigate_cas(intersect_distance, a_p, e_p, i_p, W_p, w_p, new_secondaries_3, days)

				# # check if overlap_sets is empty
				# if not overlap_sets:
				# 	overlap_sets_empty = True

				# else:
				# 	overlap_sets_empty = False	

				## Create output paths

				# investigate close approach secondaries
				investigate_cas_directory = "{}/close_approach_summaries/".format(output_path)
				caJsonPath = "{}{}_{}.json".format(investigate_cas_directory, "caInvestigate", idx)

				## Print Results

				# print investigate close approach secondaries if it isn't empty
				#if overlap_sets_empty is False:
				if overlap_sets:
					investigateCasPrinterJson(jd_p, a_p, e_p, i_p, W_p, w_p, M_p, new_secondaries_3, overlap_sets, caJsonPath)
					# except IndexError:
					# 	print(overlap_sets)
					# 	exit()

				# combine coplanar secondaries from all prefilters
				coplanar_secondaries = coplanar_secondaries_2 + coplanar_secondaries_3

			else:

				new_secondaries_3 = []
				coplanar_secondaries = coplanar_secondaries_2	

		else:

			new_secondaries_2 = []
			new_secondaries_3 = []
			coplanar_secondaries = []	

		return [[len(new_secondaries_1), len(new_secondaries_2), len(new_secondaries_3)], [len(coplanar_secondaries)]]

	def preventDoubleCount(primariesList, secondariesList):
		"""Function to generate a dictionary of primary and secondary lists in order to prevent double counting in HERA when the primaries and secondaries are the same set of objects"""

		cases2run = {}
		caseDictionary = {}

		for i, dummy in enumerate(primariesList, start = 0):

			secondaries2run = []

			for j, dummy in enumerate(secondariesList, start = i):

				if j >= len(primariesList):
					break
				
				secondaries2run.append(j)
			
			cases2run["{}".format(i)] = secondaries2run
		
		# loop through cases to run and generate lists of primaries and secondaries
		for primaryIdx, secondaryIdxs in cases2run.items():

			# define case dictionary (specifies 1 single primary)
			case = {}

			primary = primariesList[int(primaryIdx)]

			secondaries = []

			for secondaryIdx in secondaryIdxs:

				secondaries.append(secondariesList[secondaryIdx])

			case["primary"]    = primary
			case["secondaries"] = secondaries

			# add case to dictionary of cases
			caseDictionary["{}".format(primaryIdx)] = case

		return caseDictionary		

	### Outline Algorithm
	
	# define if primaries and secondaries are duplicates
	duplicates = True					

	## Read settings file
	settings = readJsonSettings(settings_path)

	## Define Inputs

	# Specify input paths
	primary_path   = settings["paths"]["primaryPath"]
	secondary_path = settings["paths"]["secondaryPath"]

	# Analysis settings
	intersect_distance = settings["analysisSettings"]["intersectDistance"]
	days               = settings["analysisSettings"]["days"]

	# Output path
	output_path = settings["paths"]["outputPath"]

	# Define Primaries and Secondaries
	data = load_inputs(primary_path, secondary_path)
	primaries = data[0]
	secondaries = data[1]

	## Clean directories before running

	# close approach summaries
	clean_directory("{}/close_approach_summaries/".format(output_path))

	## Define list of inputs
	Input = []

	if duplicates is True:

		# define list of secondary sets
		cases = preventDoubleCount(primariesList = primaries, secondariesList = secondaries)

		# Run loop and populate list by adding primary and index

		for idx, case in cases.items():

			# define current values of index and primaries
			entry = [idx, case["primary"], case["secondaries"]]

			# add entry to input list
			Input.append(entry)

	elif duplicates is False:

		# Run loop and populate list by adding primary and index
		for idx, line in enumerate(primaries, start = 1):

			# define current values of index and primaries
			entry = [idx, line, secondaries]

			# add entry to input list
			Input.append(entry)

	## Run hoots method in parallel
	
	# Count number of cores
	num_cores = multiprocessing.cpu_count()

	# Run processes
	summary = Parallel(n_jobs = num_cores)(delayed(processInput)(i) for i in Input)

	## Create summary
	total_close_approaches = create_summary(output_path, summary)

	## Print results to command window
	print(" ")
	print(" ")
	print("=================================================================================================================================================================================")
	print(" ")
	print("RESULTS FOR HERA")
	print(" ")
	print("Total number of close approaches for set: {}".format(total_close_approaches))
	print(" ")
	print("Summary of HERA run saved to:             {}".format(output_path))	
	print(" ")
	print("Number of cores utilized:                 {}".format(num_cores))
	print(" ")
	print("=================================================================================================================================================================================")
	return

#### Map command line arguments to function arguments
if __name__ == '__main__':

	run_HERA(*sys.argv[1:])
