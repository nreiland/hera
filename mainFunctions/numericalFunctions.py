## 1. Newton Raphson

# Note: this function requires that both f(x) and df(x) are defined as functions

def dx(f, x):
	return abs(0 - f(x))

def newtons_method(f, df, x0, error, max_iterations):

	delta = dx(f, x0)

	idx = 1

	while delta > error or idx  < max_iterations:

		x0 = x0 - f(x0)/df(x0)

		delta = dx(f, x0)

		idx = idx + 1

	return [x0, f(x0)];	

## 2. Multivariate Newton Raphson

def dx_multi(f, x1, x2):
	return abs(0 - f(x1, x2));

def multivariate_newtons_method(f1, f2, df1_dx1, df1_dx2, df2_dx1, df2_dx2, x1_0, x2_0, error, max_iterations):
	#print(" ")
	#print(" ")
	#print("===========================================================")
	#print("{}, {}".format(x1_0, x2_0))
	delta1 = dx_multi(f1, x1_0, x2_0)
	delta2 = dx_multi(f2, x1_0, x2_0)

	idx = 1

	while delta1 > error and delta2 > error:

		x1_0 = x1_0 + (f1(x1_0, x2_0)*df2_dx2(x1_0, x2_0) - f2(x1_0, x2_0)*df1_dx2(x1_0, x2_0)) / (df1_dx2(x1_0, x2_0)*df2_dx1(x1_0, x2_0) - df1_dx1(x1_0, x2_0)*df2_dx2(x1_0, x2_0))
		x2_0 = x2_0 + (f2(x1_0, x2_0)*df1_dx1(x1_0, x2_0) - f1(x1_0, x2_0)*df2_dx1(x1_0, x2_0)) / (df1_dx2(x1_0, x2_0)*df2_dx1(x1_0, x2_0) - df1_dx1(x1_0, x2_0)*df2_dx2(x1_0, x2_0))

		delta1 = dx_multi(f1, x1_0, x2_0)
		delta2 = dx_multi(f2, x1_0, x2_0)

		#print("{}, {}".format(x1_0, x2_0))

		idx = idx + 1

		if idx > max_iterations:

			print("Solution is unconverged after {} iterations with a tolerance of {}".format(max_iterations, error))

			break

	return [x1_0, x2_0]