"""Module containing functions required for hera close approach prediction software"""

# import packages
import numpy as np
from mainFunctions.astroFunctions import E, M, f_from_t

def readJsonSettings(settingsPath = "mainFunctions/in/settings.json"):
	"""Function to read hera json settings file"""

	# import packages
	import json

	# load settings
	with open(settingsPath, "r") as f_obj:
		jsonSettings = json.load(f_obj)

	return jsonSettings	

def load_inputs(primary_path, secondary_path):
	"""Function to load primary and secondary inputs specified in settings filee"""

	# Create Lists
	primaries = []
	secondaries = []

	# Load Primary Object File:
	with open(primary_path) as f_obj:
		lines = f_obj.readlines()
		lines = lines[2:len(lines)]

		# Load Data Into List
		for line in lines:
			data = [x.strip() for x in line.split(',')]
			primaries.append(data)

	# Load Secondary Object File:
	with open(secondary_path) as f_obj:
		lines = f_obj.readlines()
		lines = lines[2:len(lines)]

		# Load Data Into List
		for line in lines:
			data = [x.strip() for x in line.split(',')]
			secondaries.append(data)				

	return [primaries, secondaries]

def calc_delta_trig_funcs(mag_K, i_p, W_p, i_s, W_s):
	"""Function for calculating trigonometric functions of delta angle (see documentation)"""

	# Calculate trig functions of delta for primary object
	cos_delta_p = (1/mag_K)*(np.sin(i_p)*np.cos(i_s) - np.sin(i_s)*np.cos(i_p)*np.cos(W_p - W_s))
	sin_delta_p = (1/mag_K)*(np.sin(i_s)*np.sin(W_p - W_s))

	# Calculate trig functions of delta for secondary object
	cos_delta_s = (1/mag_K)*(np.sin(i_p)*np.cos(i_s)*np.cos(W_p - W_s) - np.sin(i_s)*np.cos(i_p))
	sin_delta_s = (1/mag_K)*(np.sin(i_p)*np.sin(W_p - W_s))

	# Check for bad floats
	if abs(cos_delta_p) > 1 and abs(abs(cos_delta_p) - 1) < 1e-4:

		if cos_delta_p > 0:

			cos_delta_p = 1

		elif cos_delta_p < 0:

			cos_delta_p = -1

	if abs(sin_delta_p) > 1 and abs(abs(sin_delta_p) - 1) < 1e-4:

		if sin_delta_p > 0:

			sin_delta_p = 1

		elif sin_delta_p < 0:

			sin_delta_p = -1

	if abs(cos_delta_s) > 1 and abs(abs(cos_delta_s) - 1) < 1e-4:

		if cos_delta_s > 0:

			cos_delta_s = 1

		elif cos_delta_s < 0:

			cos_delta_s = -1

	if abs(sin_delta_s) > 1 and abs(abs(sin_delta_s) - 1) < 1e-4:

		if sin_delta_s > 0:

			sin_delta_s = 1

		elif sin_delta_s < 0:

			sin_delta_s = -1

	# Calculate principal values of delta for primary object
	arccos_delta_p = np.arccos(cos_delta_p)
	arcsin_delta_p = np.arcsin(sin_delta_p)

	# Calculate principal values of delta for primary object
	arccos_delta_s = np.arccos(cos_delta_s)
	arcsin_delta_s = np.arcsin(sin_delta_s)

	return [arccos_delta_p, arcsin_delta_p, arccos_delta_s, arcsin_delta_s]

# Define function to calculate a unique delta
def unique_delta(delta_arcsin, delta_arccos, n, tolerance):
	""" Function to calculate unique values of the angle delte (see documentation)"""

	## Define outputs
	delta_Arccos_solutions = []
	delta_Arcsin_solutions = []
	delta = []

	## Determine range of solutions for Arccos(x)
	# Add primary value to list
	delta_Arccos_solutions.append(delta_arccos)

	# Determine secondary delta from arccos
	delta_2_arccos = 2*np.pi - delta_arccos

	# Add secondary value to list
	delta_Arccos_solutions.append(delta_2_arccos)

	# Calculate all other values for Arccos(x)
	i = 1
	while i <= n:

		# Calculate additional values
		delta_arccos_n1       = delta_arccos + 2*np.pi*i
		delta_arccos_n2       = delta_2_arccos + 2*np.pi*i
		delta_arccos_n1_minus = delta_arccos - 2*np.pi*i
		delta_arccos_n2_minus = delta_2_arccos - 2*np.pi*i

		# Add solutions to list
		delta_Arccos_solutions.append(delta_arccos_n1)
		delta_Arccos_solutions.append(delta_arccos_n2)
		delta_Arccos_solutions.append(delta_arccos_n1_minus)
		delta_Arccos_solutions.append(delta_arccos_n2_minus)

		# Index
		i = i + 1

	## Determine range of solutions for Arcsin(x)
	# Add primary value to list
	delta_Arcsin_solutions.append(delta_arcsin)

	# Determine secondary delta from arcsin
	delta_2_arcsin = np.pi - delta_arcsin

	# Add secondary value to list
	delta_Arcsin_solutions.append(delta_2_arcsin)

	# Calculate all other values for Arcsin(x)
	i = 1
	while i <= n:

		# Calculate additional values
		delta_arcsin_n1       = delta_arcsin + 2*np.pi*i
		delta_arcsin_n2       = delta_2_arcsin + 2*np.pi*i
		delta_arcsin_n1_minus = delta_arcsin - 2*np.pi*i
		delta_arcsin_n2_minus = delta_2_arcsin - 2*np.pi*i

		# Add solutions to list
		delta_Arcsin_solutions.append(delta_arcsin_n1)
		delta_Arcsin_solutions.append(delta_arcsin_n2)
		delta_Arcsin_solutions.append(delta_arcsin_n1_minus)
		delta_Arcsin_solutions.append(delta_arcsin_n2_minus)

		# Index
		i = i + 1

	# Compare values
	for i, dummy in enumerate(delta_Arcsin_solutions):

		for j, dummy2 in enumerate(delta_Arccos_solutions):

			diff = delta_Arcsin_solutions[i] - 	delta_Arccos_solutions[j]

			if abs(diff) <= tolerance:

				sol = (delta_Arcsin_solutions[i] + delta_Arccos_solutions[j])/2

				delta.append(sol)	

	# add condition for indexing error
	try:
		if delta[0] < 0:
			sol = delta[0] + 2*np.pi

		elif delta[0] > 2*np.pi:
			sol = delta[0] - 2*np.pi

		else:
			sol = delta[0]						

		return sol		

	except IndexError:
		print("n: {}".format(n))
		print("tol: {}".format(tolerance))
		print("delta_arsin_solutions")
		for line in delta_Arcsin_solutions:
			print(line)
		for line in delta_Arccos_solutions:
			print(line)		 	

# Define Geometric Filter 1: Perigee-Apogee Test
def prefilter1(distance, a_p, e_p, secondaries):
	"""Function defining the first geometrical prefilter"""

	# Define new secondary list
	new_secondaries = []
	discarded_secondaries = []

	# Loop through and pass secondary objects through filter
	for line in secondaries:

		# define inputs
		a_s = float(line[1])
		e_s = float(line[2])
		
		# define apogee and perigee height of primary object
		A_p = a_p * (1 + e_p)		# apogee 
		P_p = a_p * (1 - e_p)		# perigee

		# define apogee and perigee height of secondary object
		A_s = a_s * (1 + e_s)			# apogee
		P_s = a_s * (1 - e_s)			# perigee

		# determine max and min perigee/apogee heights
		Q = min(A_p, A_s)						# set Q to be the smaller apogee altitude of the two objects
		q = max(P_p, P_s)						# set q to be the larger perigee altitude of the two objects

		# perform test
		value = q - Q

		if value > distance:

			# add safe secondary into discarded secondary list
			discarded_secondaries.append(line)

		else:

			# add unsafe secondary to be considered into new secondaries list
			new_secondaries.append(line)

			# print to test
			#print("secondary must be further considered")

	return [new_secondaries, discarded_secondaries]

# Define Geometric Filter 2: Relative Geometry of Two Ellipses in Space
def prefilter2(distance, a_p, e_p, i_p, W_p, w_p, secondaries):
	"""Function defining the second geometrical prefilter"""

	### Define Functions

	## Primary Functions

	# 1. Compute relative inclination between planes(i_r) and the magnitude of the K vector(mag_K)
	def compute_ir_and_mag_K(W_p, i_p, W_s, i_s):
		"""Function for computing relative inclination and the magnitude of the intersection vector"""

		## Calculate unit vector normal to orbital plane of primary object
		u_n_p = np.array([np.sin(W_p)*np.sin(i_p), np.cos(W_p)*np.sin(i_p), np.cos(i_p)])

		# Calculate unit vector normal to orbital plane of secondary object
		u_n_s = np.array([np.sin(W_s)*np.sin(i_s), np.cos(W_s)*np.sin(i_s), np.cos(i_s)])

		# Calculate vector K, which lies along the line of intersection of the two orbital planes
		K = np.cross(u_n_s, u_n_p)

		# Calculate relative inclination
		mag_K = (K[0]**2 + K[1]**2 + K[2]**2)**(1/2)		# magnitude of K vector
		i_r = np.arcsin(mag_K)

		return [i_r, mag_K]

	# 3. Calculate point of closet approach
	def calc_poca(f_p_star, f_s_star, a_p, e_p, w_p, a_s, e_s, w_s, delta_p, delta_s, i_r):
		"""Function to calculate the point of closest approach between satellites"""

		# define constants
		mu = 3.9860044e5

		# calculate Ur angles
		U_r_p = f_p_star + w_p - delta_p

		U_r_s = f_s_star + w_s - delta_s

		# calculate cos_gamma
		cos_gamma = np.cos(U_r_p)*np.cos(U_r_s) + np.sin(U_r_p)*np.sin(U_r_s)*np.cos(i_r)

		# calculate magnitude of radius vectors for primary and secondary objects

		# primary
		h_p = (mu*a_p*(1 - e_p**2))**(1/2)

		r_p = ((h_p**2)/mu)*(1/(1 + e_p*np.cos(f_p_star)))

		# secondary
		h_s = (mu*a_s*(1 - e_s**2))**(1/2)

		r_s = ((h_s**2)/mu)*(1/(1 + e_s*np.cos(f_s_star)))

		# check if solution will be undefined
		if (r_p**2 + r_s**2 - 2*r_p*r_s*cos_gamma) < 0:

			if abs(r_p**2 + r_s**2 - 2*r_p*r_s*cos_gamma) < 1e-7:

				# set value equal to zero if arbitrarily close to zero
				poca = 0

			else:
			
				print("error check tol")

		else:	

			# calculate relative separation distance
			poca = (r_p**2 + r_s**2 - 2*r_p*r_s*cos_gamma)**(1/2)		

		return poca 	

	# 4. Function to employ newton-raphson for root finding
	def calc_fp_fs(a_p, e_p, w_p, f_p_0_1, delta_p, a_s, e_s, w_s, f_s_0_1, delta_s, i_r, tol = 1e-8):
		"""Function using newton-raphson to determine true anomalies at wich the point of closest approach occurs"""

		# initial guesses
		f_p        = f_p_0_1
		f_s        = f_s_0_1
		iterations = 0 

		while iterations < 500:

			# define constants
			mu = 3.9860044e5

			# calculate Ur angles
			U_r_p = f_p + w_p - delta_p

			U_r_s = f_s + w_s - delta_s

			# calculate cos_gamma
			cos_gamma = np.cos(U_r_p)*np.cos(U_r_s) + np.sin(U_r_p)*np.sin(U_r_s)*np.cos(i_r)

			# calculate eccentric anomaly
			E_p = 2*np.arctan(((1 - e_p)/(1 + e_p))**(1/2)*np.tan((1/2)*f_p))
			E_s = 2*np.arctan(((1 - e_s)/(1 + e_s))**(1/2)*np.tan((1/2)*f_s))

			# calculate ax and ay values
			a_x_p = e_p * np.cos(w_p - delta_p)
			a_y_p = e_p * np.sin(w_p - delta_p)
			a_x_s = e_s * np.cos(w_s - delta_s)
			a_y_s = e_s * np.sin(w_s - delta_s)

			# calculate magnitude of radius vectors for primary and secondary objects

			# primary
			h_p = (mu*a_p*(1 - e_p**2))**(1/2)

			r_p = ((h_p**2)/mu)*(1/(1 + e_p*np.cos(f_p)))

			# secondary
			h_s = (mu*a_s*(1 - e_s**2))**(1/2)

			r_s = ((h_s**2)/mu)*(1/(1 + e_s*np.cos(f_s)))

			# calculate helper functions
			A = np.sin(U_r_p) + a_y_p
			B = np.cos(U_r_p) + a_x_p
			C = np.sin(U_r_s) + a_y_s
			D = np.cos(U_r_s) + a_x_s

			# calculate primary functions
			F      = r_p*e_p*f_p*np.sin(f_p) + r_s*(A*np.cos(U_r_s) - B*np.cos(i_r)*np.sin(U_r_s))
			G      = r_s*e_s*f_s*np.sin(f_s) + r_p*(C*np.cos(U_r_p) - D*np.cos(i_r)*np.sin(U_r_p))
			dF_dfp = r_p*e_p*np.cos(E_p) + r_s*cos_gamma
			dF_dfs = -1*(r_s/(1 + e_s*np.cos(f_s)))*(A*C + B*D*np.cos(i_r))
			dG_dfp = -1*(r_p/(1 + e_p*np.cos(f_p)))*(A*C + B*D*np.cos(i_r))
			dG_dfs = r_s*e_s*np.cos(E_s) + r_p*cos_gamma

			# calculte h and k
			h = (F*dG_dfs - G*dF_dfs)/(dF_dfs*dG_dfp - dF_dfp*dG_dfs)
			k = (G*dF_dfp - F*dG_dfp)/(dF_dfs*dG_dfp - dF_dfp*dF_dfs)

			# calculate new true anomaly values
			f_p_new = f_p + h
			f_s_new = f_s + k

			# update new values
			f_p        = f_p_new
			f_s        = f_s_new
			iterations = iterations + 1

			if abs(f_p_new - f_p) <= tol and abs(f_s_new - f_s) <= tol:

				break

			elif iterations == 500:

				print("solution is unconverged")

				break	

		return [f_p, f_s] 		

	### Outline Algorithm

	## Create output lists
	discarded_secondaries = []
	new_secondaries       = []
	coplanar_secondaries  = []

	## Convert primary angles from degrees to radians
	i_p = np.deg2rad(i_p)		# inclination
	W_p = np.deg2rad(W_p)		# RAAN
	w_p = np.deg2rad(w_p)		# argument of perigee

	## Loop through secondary objects and compare values
	for line in secondaries:

		# Load Keplarin elements for second object
		a_s = float(line[1])
		e_s = float(line[2])
		i_s = float(line[3])
		W_s = float(line[4])
		w_s = float(line[5])

		# Convert secondary angles from degrees to radians
		i_s = np.deg2rad(i_s)		# inclination
		W_s = np.deg2rad(W_s)		# RAAN
		w_s = np.deg2rad(w_s)		# argument of perigee

		# Compute relative inclination between planes(i_r) and the magnitude of the K vector(mag_K)
		output = compute_ir_and_mag_K( W_p = W_p, i_p = i_p, W_s = W_s, i_s = i_s)
		i_r = output[0]
		mag_K = output[1]	

		# Check if secondary and primary are coplanar
		if i_r <= 1e-8:	# secondary is coplanar

			# add safe secondary into coplanr secondary list
			coplanar_secondaries.append(line)

		else:			# secondary is not coplanar

			# Calculate solutions of trig functions for delta angles	
			output = calc_delta_trig_funcs(mag_K = mag_K, i_p = i_p, W_p = W_p, i_s = i_s, W_s = W_s)
			arccos_delta_p = output[0]
			arcsin_delta_p = output[1]
			arccos_delta_s = output[2]
			arcsin_delta_s = output[3]

			# Calculate delta for primary obejct
			delta_p = unique_delta(delta_arcsin = arcsin_delta_p, delta_arccos = arccos_delta_p, n = 1, tolerance = 0.001)
			#delta_p = delta_output_p[0]

			# Calculate delta for secondary object
			delta_s = unique_delta(delta_arcsin = arcsin_delta_s, delta_arccos = arccos_delta_s, n = 1, tolerance = 0.001)
			#delta_s = delta_output_s[0]

			# Set intial values for solution 1
			f_p_0_1 = delta_p - w_p 
			f_s_0_1 = delta_s - w_s 

			# Run multi-variate Newton's method for solution 1
			output     = calc_fp_fs(a_p, e_p, w_p, f_p_0_1, delta_p, a_s, e_s, w_s, f_s_0_1, delta_s, i_r, tol = 1e-8)
			f_p_star_1 = output[0]
			f_s_star_1 = output[1]

			# Set intial values for solution 2
			f_p_0_2 = f_p_star_1 + np.pi
			f_s_0_2 = f_s_star_1 + np.pi

			# Run multi-variate Newton's method for solution 2
			output     = calc_fp_fs(a_p, e_p, w_p, f_p_0_2, delta_p, a_s, e_s, w_s, f_s_0_2, delta_s, i_r, tol = 1e-8)
			f_p_star_2 = output[0]
			f_s_star_2 = output[1]

			# Calculte points of closest approach
			poca_1 = calc_poca(f_p_star_1, f_s_star_1, a_p, e_p, w_p, a_s, e_s, w_s, delta_p, delta_s, i_r)	
			poca_2 = calc_poca(f_p_star_2, f_s_star_2, a_p, e_p, w_p, a_s, e_s, w_s, delta_p, delta_s, i_r)

			# Compare points of closest approach to allowable distance
			if poca_1 > distance and poca_2 > distance:

				# add safe secondary into discarded secondary list
				discarded_secondaries.append(line)

			else:

				# add unsafe secondary to be considered into new secondaries list
				new_secondaries.append(line)

	return [new_secondaries, discarded_secondaries, coplanar_secondaries]

def time_prefilter(distance, a_p, e_p, i_p, W_p, w_p, secondaries, days):
	"""Function defining time prefilter"""

	### Define Functions

	# 1. Geometrically compute delta values for primary and secondary objects
	def compute_deltas(W_p, i_p, W_s, i_s):
		"""Function for computing delta values (see documentation)"""
			
		# Calculate unit vector normal to orbital plane of primary object
		u_n_p = np.array([np.sin(W_p)*np.sin(i_p), np.cos(W_p)*np.sin(i_p), np.cos(i_p)])	

		# Calculate unit vector normal to orbital plane of secondary object
		u_n_s = np.array([np.sin(W_s)*np.sin(i_s), np.cos(W_s)*np.sin(i_s), np.cos(i_s)])

		# Calculate vector K, which lies along the line of intersection of the two orbital planes
		K = np.cross(u_n_s, u_n_p)

		# Calculate relative inclination
		mag_K = (K[0]**2 + K[1]**2 + K[2]**2)**(1/2)		# magnitude of K vector

		# Calculate solutions of trig functions for delta angles	
		output = calc_delta_trig_funcs(mag_K = mag_K, i_p = i_p, W_p = W_p, i_s = i_s, W_s = W_s)
		arccos_delta_p = output[0]
		arcsin_delta_p = output[1]
		arccos_delta_s = output[2]
		arcsin_delta_s = output[3]

		# Calculate delta for primary obejct
		delta_p = unique_delta(delta_arcsin = arcsin_delta_p, delta_arccos = arccos_delta_p, n = 1, tolerance = 0.001)
		#delta_p = delta_output_p[0]

		# Calculate delta for secondary object
		delta_s = unique_delta(delta_arcsin = arcsin_delta_s, delta_arccos = arccos_delta_s, n = 1, tolerance = 0.001)

		return [delta_p, delta_s]

	# 2. Calculate cossin of phase of primary and secondary objects
	def calc_cos_urs(a, e, w, delta, D):
		"""Function for calculating the angle U_r between the line of intersection and radius vector"""

		# Define geometric sub-functions for primary object
		a_x = e*np.cos(w - delta)
		a_y = e*np.sin(w - delta)

		alpha = a*(1 - (e**2))

		# Define Q
		Q = alpha*(alpha - 2*D*a_y) - (1 - (e**2))*(D**2)

		# Define cos(ur)
		cos_ur_plus  = (-(D**2)*a_x + (alpha - D*a_y)*(Q**(1/2))) / (alpha*(alpha - 2*D*a_y) + (D**2)*(e**2))
		cos_ur_minus = (-(D**2)*a_x - (alpha - D*a_y)*(Q**(1/2))) / (alpha*(alpha - 2*D*a_y) + (D**2)*(e**2))

		# Check if time filter can be applied
		if Q < 0 or abs(cos_ur_plus) > 1 or abs(cos_ur_minus) > 1:

			app_time_filter = False	# time filter cannot be applied

		else:
			app_time_filter = True	# time filter can be applied

		return [cos_ur_plus, cos_ur_minus, app_time_filter]

	# 3. Compute time windows for primary and secondary objects		
	def compute_time_window(cos_ur_plus, cos_ur_minus, w, a, e, delta):
		"""Function for computing time windows"""

		# define output list
		time_windows = []

		# Compute angular window of primary object
		ur_1 = np.arccos(cos_ur_plus)
		ur_2 = 2*np.pi - np.arccos(cos_ur_plus)
		ur_3 = np.arccos(cos_ur_minus)
		ur_4 = 2*np.pi - np.arccos(cos_ur_minus)

		# Compute true anomaly window of primary object
		f_1 = ur_1 - w + delta
		f_2 = ur_2 - w + delta
		f_3 = ur_3 - w + delta
		f_4 = ur_4 - w + delta

		# Compute time window of primary object using kepler's equations
		# Mean motion
		n = (mu/(a**3))**(1/2)

		# Period 
		P = (2*np.pi)/n

		# Eccentric anomaly
		E_1 = E(f_1, e)
		E_2 = E(f_2, e)
		E_3 = E(f_3, e)
		E_4 = E(f_4, e)

		# Time windows (t = M/n)
		t_1 = M(E_1, e)/n
		t_2 = M(E_2, e)/n
		t_3 = M(E_3, e)/n
		t_4 = M(E_4, e)/n

		# format window
		if t_2  > t_1:
			win_1 = [t_1, t_2]

		elif t_1 > t_2:
			win_1 = [t_2, t_1]

		if t_4 > t_3:
			win_2 = [t_3, t_4]

		elif t_3 > t_4:
			win_2 = [t_4, t_3]
				
		# add time windows for primary object to output list
		entry = [win_1, win_2, e, a]
		time_windows.append(entry)

		## Generate sequence of time windows
		seconds_forward = days*24*60*60

		# Primary object
		time = P
		while time <= seconds_forward - P:

			# add period to windows
			t_1 = t_1 + P
			t_2 = t_2 + P
			t_3 = t_3 + P
			t_4 = t_4 + P

			# format window
			if t_2  > t_1:
				win_1 = [t_1, t_2]

			elif t_1 > t_2:
				win_1 = [t_2, t_1]

			if t_4 > t_3:
				win_2 = [t_3, t_4]

			elif t_3 > t_4:
				win_2 = [t_4, t_3]

			# create new list entry
			new_window = [win_1, win_2, e, a]
			time_windows.append(new_window)

			# index
			time = time + P

		return time_windows

	# 4. Compare time windows for primary and secondary objects
	def compare_time_windows(primary_windows, secondary_windows):
		"""Function for comparing time windows"""

		# define overlap set list
		overlap_set = []

		# grab one set of primary time windows, [t1_p, t2_p], [t3_p, t4_p]
		for line in primary_windows:

			# time window 1
			win_1_p = line[0]		# [t1_p, t2_p]
			t1_p    = win_1_p[0]
			t2_p    = win_1_p[1]

			# time window 2
			win_2_p = line[1]		# [t3_p, t4_p]
			t3_p    = win_2_p[0]
			t4_p    = win_2_p[1]

			# eccentricity
			e_p = line[2]

			# semi-major axis
			a_p = line[3]

			# grab one set of secondary time windows to compare to the set of primrary time windows, [t1_s, t2_s], [t3_s, t4_s]
			for entry in secondary_windows:

				# time window 1
				win_1_s = entry[0]		# [t1_p, t2_p]
				t1_s    = win_1_s[0]
				t2_s    = win_1_s[1]

				# time window 2
				win_2_s = entry[1]		# [t3_p, t4_p]
				t3_s    = win_2_s[0]
				t4_s    = win_2_s[1]

				# eccentricity
				e_s = entry[2]

				# semi-major axis
				a_s = entry[3]

				# Check if windows overlap

				# check if [t1_s, t2_s] overlaps with [t1_p, t2_p] | (cases 1, 2, and 3)

				# check subcase 1:
				if t1_s <= t1_p and t1_p <= t2_s:
					case1 = True   		# overlap
				else:
					case1 = False 		# no overlap
				
				# check subcase 2:			
				if t1_p <= t1_s and t1_s <= t2_p and t1_p <= t2_s and t2_s <= t2_p: 
					case2 = True   		# overlap
				else:
					case2 = False       # no overlap
				
				# check subcase 3:	
				if t1_s <= t2_p and t2_p <= t2_s:
					case3 = True   		# overlap
				else:
					case3 = False		# no overlap
				
				# check if [t3_s, t4_s] overlaps with [t3_p, t4_p] | (cases 4, 5, and 6) 

				# check subcase 4	
				if t3_s <= t3_p and t3_p <= t4_s:
					case4 = True   		# overlap
				else:
					case4 = False    	# no overlap

				# check subcase 5
				if t3_p <= t3_s and t3_s <= t4_p and t3_p <= t4_s and t4_s <= t4_p:
					case5 = True         # overlap
				else:
					case5 = False        # no overlap

				# check subcase 6
				if t3_s <= t4_p and t4_p <= t4_s:
					case6 = True   					# overlap
				else:
					case6 = False					# no overlap				

				# if [t1_s, t2_s] overlaps with [t1_p, t2_p] append the overlap set list with the corresponding time windows
				if case1 is True or case2 is True or case3 is True:

					# calculate true anomalies of primary object
					f1_p = np.rad2deg(f_from_t(t = t1_p, e = e_p, a = a_p, mu = mu))
					f2_p = np.rad2deg(f_from_t(t = t2_p, e = e_p, a = a_p, mu = mu))

					# calculate true anomalies of secondary object
					f1_s = np.rad2deg(f_from_t(t = t1_s, e = e_s, a = a_s, mu = mu))
					f2_s = np.rad2deg(f_from_t(t = t2_s, e = e_s, a = a_s, mu = mu))

					# define true anomaly windows
					prim_f_windows = [f1_p, f2_p]
					sec_f_windows  = [f1_s, f2_s]

					# define time windows
					prim_t_windows = [t1_p, t2_p]
					sec_t_windows  = [t1_s, t2_s]

					# create list entry
					overlap_entry = [prim_t_windows, sec_t_windows, prim_f_windows, sec_f_windows]

					# add to overlap set
					overlap_set.append(overlap_entry)

				# if [t3_s, t4_s] overlaps with [t3_p, t4_p] append the overlap set list with the corresponding time windows
				elif case4 is True or case5 is True or case6 is True:

					# calculate true anomalies of primary object
					f3_p = np.rad2deg(f_from_t(t = t3_p, e = e_p, a = a_p, mu = mu))
					f4_p = np.rad2deg(f_from_t(t = t4_p, e = e_p, a = a_p, mu = mu))

					# calculate true anomalies of secondary object
					f3_s = np.rad2deg(f_from_t(t = t3_s, e = e_s, a = a_s, mu = mu))
					f4_s = np.rad2deg(f_from_t(t = t4_s, e = e_s, a = a_s, mu = mu))

					# define true anomaly windows
					prim_f_windows = [f3_p, f4_p]
					sec_f_windows  = [f3_s, f4_s]

					# define time windows
					prim_t_windows = [t3_p, t4_p]
					sec_t_windows  = [t3_s, t4_s]

					# create list entry
					overlap_entry = [prim_t_windows, sec_t_windows, prim_f_windows, sec_f_windows]

					# add to overlap set
					overlap_set.append(overlap_entry)

				# if [t1_s, t2_s] overlaps with [t1_p, t2_p] and [t3_s, t4_s] overlaps with [t3_p, t4_p] append the overlap set list with the corresponding time windows
				elif (case1 is True or case2 is True or case3 is True) and (case4 is True or case5 is True or case6 is True):

					# calculate true anomalies of primary object
					f1_p = np.rad2deg(f_from_t(t = t1_p, e = e_p, a = a_p, mu = mu))
					f2_p = np.rad2deg(f_from_t(t = t2_p, e = e_p, a = a_p, mu = mu))
					f3_p = np.rad2deg(f_from_t(t = t3_p, e = e_p, a = a_p, mu = mu))
					f4_p = np.rad2deg(f_from_t(t = t4_p, e = e_p, a = a_p, mu = mu))

					# calculate true anomalies of secondary object
					f1_s = np.rad2deg(f_from_t(t = t1_s, e = e_s, a = a_s, mu = mu))
					f2_s = np.rad2deg(f_from_t(t = t2_s, e = e_s, a = a_s, mu = mu))
					f3_s = np.rad2deg(f_from_t(t = t3_s, e = e_s, a = a_s, mu = mu))
					f4_s = np.rad2deg(f_from_t(t = t4_s, e = e_s, a = a_s, mu = mu))

					# define true anomaly windows
					prim_f_windows = [f1_p, f2_p, f3_p, f4_p]
					sec_f_windows  = [f1_s, f2_s, f3_s, f4_s]

					# define time windows
					prim_t_windows = [t1_p, t2_p, t3_p, t4_p]
					sec_t_windows  = [t1_s, t2_s, t3_s, t4_s]

					# create list entry
					overlap_entry = [prim_t_windows, sec_t_windows, prim_f_windows, sec_f_windows]	

					# add to overlap set
					overlap_set.append(overlap_entry)

		return overlap_set						

	### Outline Algorithm
		
	## Create output lists

	# secondary object lists
	discarded_secondaries  = []
	new_secondaries        = []
	coplanar_secondaries   = []

	# create list for overlapping sets
	overlappingSecondaryWindows = []

	## Define Earth's standard gravitational parameter
	mu = 398600.4418

	## Define D distance, where D = Z*
	D = distance

	## Convert primry angles from degrees to radians
	i_p = np.deg2rad(i_p)		# inclination
	W_p = np.deg2rad(W_p)		# RAAN
	w_p = np.deg2rad(w_p)		# argument of perigee

	## Loop through secondary objects and compare values
	for line in secondaries:

		# Load Keplarin elements for second object
		a_s = float(line[1])
		e_s = float(line[2])
		i_s = float(line[3])
		W_s = float(line[4])
		w_s = float(line[5])

		# Convert secondary element angles from degrees to radians
		i_s = np.deg2rad(i_s)		# inclination
		W_s = np.deg2rad(W_s)		# RAAN
		w_s = np.deg2rad(w_s)		# argument of perigee

		# Compute deltas for primary and secodary object
		deltas = compute_deltas(W_p = W_p, i_p = i_p, W_s = W_s, i_s = i_s)
		delta_p = deltas[0]
		delta_s = deltas[1]

		# Compute phase cosine for primary object and check if time filter can be applied
		cos_urs_p = calc_cos_urs(a = a_p, e = e_p, w = w_p, delta = delta_p, D = D)
		cos_ur_p_plus = cos_urs_p[0]
		cos_ur_p_minus = cos_urs_p[1]
		app_time_filter_p = cos_urs_p[2]

		# Compute phase cosine for secondary object and check if time filter can be applied
		cos_urs_s = calc_cos_urs(a = a_s, e = e_s, w = w_s, delta = delta_s, D = D)
		cos_ur_s_plus = cos_urs_s[0]
		cos_ur_s_minus = cos_urs_s[1]
		app_time_filter_s = cos_urs_s[2]

		# Check if time filters can be applied
		if app_time_filter_p is False or app_time_filter_s is False:	
			
			# time filter cannot be applied to primary
			coplanar_secondaries.append(line)

		elif app_time_filter_p is True and app_time_filter_s is True:		# time filter can be applied to both primary and secondary

			# Compute time windows for primary object
			primary_windows = compute_time_window(cos_ur_plus = cos_ur_p_plus , cos_ur_minus = cos_ur_p_minus, w = w_p, a = a_p, e = e_p, delta = delta_p)		

			# Compute time windows for secondary object
			secondary_windows = compute_time_window(cos_ur_plus = cos_ur_s_plus , cos_ur_minus = cos_ur_s_minus, w = w_s, a = a_s, e = e_s, delta = delta_s)

			# Check if primary and secondary time windows overlap
			overlapSet = compare_time_windows(primary_windows = primary_windows, secondary_windows = secondary_windows)
			
		# If time windows overlap, add secondary orbels and overlapping windows to designated output lists						
		if not overlapSet:

			discarded_secondaries.append(line)

		else:

			new_secondaries.append(line)
			overlappingSecondaryWindows.append(overlapSet)

	# define output dictionary
	outputDic = {
		"newSecondaries": new_secondaries,
		"discardedSecondaries": discarded_secondaries,
		"coplanarSecondaries": coplanar_secondaries,
		"overlapSet": overlappingSecondaryWindows,
		}	

	return outputDic

def create_summary(summary_directory, summary):
	"""Function for creating summary of hera run"""	
	
	# Create summary path
	summary_path = "{}/summary.txt".format(summary_directory)

	# Count total close approaches
	total = 0

	for line in summary:
	
		total = total + line[0][2]

	# open output file
	with open(summary_path, 'w') as fp:
		fp.write("# HERA Batch Results\n")
		fp.write("# ===================================================================================================================\n")
		fp.write("# \n")
		fp.write("  Total number of close approaches for entire set: {}\n".format(total))
		fp.write(" \n")
		fp.write("# Stage 1 Close Approaches        Stage 2 Close Approaches        Stage 3 Close Approaches        Coplanar Secondaries\n")
		fp.write (" \n")

		# loop through output directories
		for line in summary:

			# format values
			stage1_ca = "%1.6E" % line[0][0]
			stage2_ca = "%1.6E" % line[0][1]
			stage3_ca = "%1.6E" % line[0][2]
			coplanars = "%1.6E" % line[1][0]

			# print values
			fp.write("  {}                    {}                    {}                    {}\n".format(stage1_ca, stage2_ca, stage3_ca, coplanars))

	return total
