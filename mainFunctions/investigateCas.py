"""Module used for determining time of closest approach in intersecting time windows"""
def investigate_cas(distance, a_p, e_p, i_p, W_p, w_p, secondaries, days):
	"""Main function for investigating intersecting time windows"""

	### Import Packages
	import numpy as np
	from mainFunctions.astroFunctions import E, M, f_from_t
	from mainFunctions.heraFunctions import unique_delta, calc_delta_trig_funcs

	### Define Functions

	# 1. Geometrically compute delta values for primary and secondary objects
	def compute_deltas(W_p, i_p, W_s, i_s):
		"""Function for computing delta values (see documentation)"""
			
		# Calculate unit vector normal to orbital plane of primary object
		u_n_p = np.array([np.sin(W_p)*np.sin(i_p), np.cos(W_p)*np.sin(i_p), np.cos(i_p)])	

		# Calculate unit vector normal to orbital plane of secondary object
		u_n_s = np.array([np.sin(W_s)*np.sin(i_s), np.cos(W_s)*np.sin(i_s), np.cos(i_s)])

		# Calculate vector K, which lies along the line of intersection of the two orbital planes
		K = np.cross(u_n_s, u_n_p)

		# Calculate relative inclination
		mag_K = (K[0]**2 + K[1]**2 + K[2]**2)**(1/2)		# magnitude of K vector

		# Calculate solutions of trig functions for delta angles	
		output = calc_delta_trig_funcs(mag_K = mag_K, i_p = i_p, W_p = W_p, i_s = i_s, W_s = W_s)
		arccos_delta_p = output[0]
		arcsin_delta_p = output[1]
		arccos_delta_s = output[2]
		arcsin_delta_s = output[3]

		# Calculate delta for primary obejct
		delta_p = unique_delta(delta_arcsin = arcsin_delta_p, delta_arccos = arccos_delta_p, n = 1, tolerance = 0.001)
		#delta_p = delta_output_p[0]

		# Calculate delta for secondary object
		delta_s = unique_delta(delta_arcsin = arcsin_delta_s, delta_arccos = arccos_delta_s, n = 1, tolerance = 0.001)

		return [delta_p, delta_s]

	# 2. Calculate cossin of phase of primary and secondary objects
	def calc_cos_urs(a, e, w, delta, D):
		"""Function for calculating U_r, the angle between the line of intersection and radius vector"""
			
		# Define geometric sub-functions for primary object
		a_x = e*np.cos(w - delta)
		a_y = e*np.sin(w - delta)

		alpha = a*(1 - (e**2))

		# Define Q
		Q = alpha*(alpha - 2*D*a_y) - (1 - (e**2))*(D**2)

		# Define cos(ur)
		cos_ur_plus  = (-(D**2)*a_x + (alpha - D*a_y)*(Q**(1/2))) / (alpha*(alpha - 2*D*a_y) + (D**2)*(e**2))
		cos_ur_minus = (-(D**2)*a_x - (alpha - D*a_y)*(Q**(1/2))) / (alpha*(alpha - 2*D*a_y) + (D**2)*(e**2))

		return [cos_ur_plus, cos_ur_minus]

	# 3. Compute time windows for primary and secondary objects		
	def compute_time_window(cos_ur_plus, cos_ur_minus, w, a, e, delta):
		"""Function for computing time windows"""

		# define output list
		time_windows = []

		# Compute angular window of primary object
		ur_1 = np.arccos(cos_ur_plus)
		ur_2 = 2*np.pi - np.arccos(cos_ur_plus)
		ur_3 = np.arccos(cos_ur_minus)
		ur_4 = 2*np.pi - np.arccos(cos_ur_minus)

		# Compute true anomaly window of primary object
		f_1 = ur_1 - w + delta
		f_2 = ur_2 - w + delta
		f_3 = ur_3 - w + delta
		f_4 = ur_4 - w + delta

		# Compute time window of primary object using kepler's equations
		# Mean motion
		n = (mu/(a**3))**(1/2)

		# Period 
		P = (2*np.pi)/n

		# Eccentric anomaly
		E_1 = E(f_1, e)
		E_2 = E(f_2, e)
		E_3 = E(f_3, e)
		E_4 = E(f_4, e)

		# Time windows (t = M/n)
		t_1 = M(E_1, e)/n
		t_2 = M(E_2, e)/n
		t_3 = M(E_3, e)/n
		t_4 = M(E_4, e)/n

		# format window
		if t_2  > t_1:
			win_1 = [t_1, t_2]

		elif t_1 > t_2:
			win_1 = [t_2, t_1]

		if t_4 > t_3:
			win_2 = [t_3, t_4]

		elif t_3 > t_4:
			win_2 = [t_4, t_3]
				
		# add time windows for primary object to output list
		entry = [win_1, win_2, e, a]
		time_windows.append(entry)

		## Generate sequence of time windows
		seconds_forward = days*24*60*60

		# Primary object
		time = P
		while time <= seconds_forward - P:

			# add period to windows
			t_1 = t_1 + P
			t_2 = t_2 + P
			t_3 = t_3 + P
			t_4 = t_4 + P

			# format window
			if t_2  > t_1:
				win_1 = [t_1, t_2]

			elif t_1 > t_2:
				win_1 = [t_2, t_1]

			if t_4 > t_3:
				win_2 = [t_3, t_4]

			elif t_3 > t_4:
				win_2 = [t_4, t_3]

			# create new list entry
			new_window = [win_1, win_2, e, a]
			time_windows.append(new_window)

			# index
			time = time + P

		return time_windows

	# 4. Compare time windows for primary and secondary objects
	def compare_time_windows(primary_windows, secondary_windows):
		"""Function for comparing time windows"""

		# define overlap set list
		overlap_set = []

		# grab one set of primary time windows, [t1_p, t2_p], [t3_p, t4_p]
		for line in primary_windows:

			# time window 1
			win_1_p = line[0]		# [t1_p, t2_p]
			t1_p    = win_1_p[0]
			t2_p    = win_1_p[1]

			# time window 2
			win_2_p = line[1]		# [t3_p, t4_p]
			t3_p    = win_2_p[0]
			t4_p    = win_2_p[1]

			# eccentricity
			e_p = line[2]

			# semi-major axis
			a_p = line[3]

			# grab one set of secondary time windows to compare to the set of primrary time windows, [t1_s, t2_s], [t3_s, t4_s]
			for entry in secondary_windows:

				# time window 1
				win_1_s = entry[0]		# [t1_p, t2_p]
				t1_s    = win_1_s[0]
				t2_s    = win_1_s[1]

				# time window 2
				win_2_s = entry[1]		# [t3_p, t4_p]
				t3_s    = win_2_s[0]
				t4_s    = win_2_s[1]

				# eccentricity
				e_s = entry[2]

				# semi-major axis
				a_s = entry[3]

				# Check if windows overlap

				# check if [t1_s, t2_s] overlaps with [t1_p, t2_p] | (cases 1, 2, and 3)

				# check subcase 1:
				if t1_s <= t1_p and t1_p <= t2_s:
					case1 = True   		# overlap
				else:
					case1 = False 		# no overlap
				
				# check subcase 2:			
				if t1_p <= t1_s and t1_s <= t2_p and t1_p <= t2_s and t2_s <= t2_p: 
					case2 = True   		# overlap
				else:
					case2 = False       # no overlap
				
				# check subcase 3:	
				if t1_s <= t2_p and t2_p <= t2_s:
					case3 = True   		# overlap
				else:
					case3 = False		# no overlap
				
				# check if [t3_s, t4_s] overlaps with [t3_p, t4_p] | (cases 4, 5, and 6) 

				# check subcase 4	
				if t3_s <= t3_p and t3_p <= t4_s:
					case4 = True   		# overlap
				else:
					case4 = False    	# no overlap

				# check subcase 5
				if t3_p <= t3_s and t3_s <= t4_p and t3_p <= t4_s and t4_s <= t4_p:
					case5 = True         # overlap
				else:
					case5 = False        # no overlap

				# check subcase 6
				if t3_s <= t4_p and t4_p <= t4_s:
					case6 = True   					# overlap
				else:
					case6 = False					# no overlap				

				# if [t1_s, t2_s] overlaps with [t1_p, t2_p] append the overlap set list with the corresponding time windows
				if case1 is True or case2 is True or case3 is True:

					# calculate true anomalies of primary object
					f1_p = np.rad2deg(f_from_t(t = t1_p, e = e_p, a = a_p, mu = mu))
					f2_p = np.rad2deg(f_from_t(t = t2_p, e = e_p, a = a_p, mu = mu))

					# calculate true anomalies of secondary object
					f1_s = np.rad2deg(f_from_t(t = t1_s, e = e_s, a = a_s, mu = mu))
					f2_s = np.rad2deg(f_from_t(t = t2_s, e = e_s, a = a_s, mu = mu))

					# define true anomaly windows
					prim_f_windows = [f1_p, f2_p]
					sec_f_windows  = [f1_s, f2_s]

					# define time windows
					prim_t_windows = [t1_p, t2_p]
					sec_t_windows  = [t1_s, t2_s]

					# create list entry
					overlap_entry = [prim_t_windows, sec_t_windows, prim_f_windows, sec_f_windows]

					# add to overlap set
					overlap_set.append(overlap_entry)

				# if [t3_s, t4_s] overlaps with [t3_p, t4_p] append the overlap set list with the corresponding time windows
				elif case4 is True or case5 is True or case6 is True:

					# calculate true anomalies of primary object
					f3_p = np.rad2deg(f_from_t(t = t3_p, e = e_p, a = a_p, mu = mu))
					f4_p = np.rad2deg(f_from_t(t = t4_p, e = e_p, a = a_p, mu = mu))

					# calculate true anomalies of secondary object
					f3_s = np.rad2deg(f_from_t(t = t3_s, e = e_s, a = a_s, mu = mu))
					f4_s = np.rad2deg(f_from_t(t = t4_s, e = e_s, a = a_s, mu = mu))

					# define true anomaly windows
					prim_f_windows = [f3_p, f4_p]
					sec_f_windows  = [f3_s, f4_s]

					# define time windows
					prim_t_windows = [t3_p, t4_p]
					sec_t_windows  = [t3_s, t4_s]

					# create list entry
					overlap_entry = [prim_t_windows, sec_t_windows, prim_f_windows, sec_f_windows]

					# add to overlap set
					overlap_set.append(overlap_entry)

				# if [t1_s, t2_s] overlaps with [t1_p, t2_p] and [t3_s, t4_s] overlaps with [t3_p, t4_p] append the overlap set list with the corresponding time windows
				elif (case1 is True or case2 is True or case3 is True) and (case4 is True or case5 is True or case6 is True):

					# calculate true anomalies of primary object
					f1_p = np.rad2deg(f_from_t(t = t1_p, e = e_p, a = a_p, mu = mu))
					f2_p = np.rad2deg(f_from_t(t = t2_p, e = e_p, a = a_p, mu = mu))
					f3_p = np.rad2deg(f_from_t(t = t3_p, e = e_p, a = a_p, mu = mu))
					f4_p = np.rad2deg(f_from_t(t = t4_p, e = e_p, a = a_p, mu = mu))

					# calculate true anomalies of secondary object
					f1_s = np.rad2deg(f_from_t(t = t1_s, e = e_s, a = a_s, mu = mu))
					f2_s = np.rad2deg(f_from_t(t = t2_s, e = e_s, a = a_s, mu = mu))
					f3_s = np.rad2deg(f_from_t(t = t3_s, e = e_s, a = a_s, mu = mu))
					f4_s = np.rad2deg(f_from_t(t = t4_s, e = e_s, a = a_s, mu = mu))

					# define true anomaly windows
					prim_f_windows = [f1_p, f2_p, f3_p, f4_p]
					sec_f_windows  = [f1_s, f2_s, f3_s, f4_s]

					# define time windows
					prim_t_windows = [t1_p, t2_p, t3_p, t4_p]
					sec_t_windows  = [t1_s, t2_s, t3_s, t4_s]

					# create list entry
					overlap_entry = [prim_t_windows, sec_t_windows, prim_f_windows, sec_f_windows]	

					# add to overlap set
					overlap_set.append(overlap_entry)

		return overlap_set								

	### Outline Algorithm

	# Define Outputs
	overlap_sets = []

	## Define Earth's standard gravitational parameter
	mu = 398600.4418

	## Define D distance, where D = Z*
	D = distance

	## Convert primry angles from degrees to radians
	i_p = np.deg2rad(i_p)		# inclination
	W_p = np.deg2rad(W_p)		# RAAN
	w_p = np.deg2rad(w_p)		# argument of perigee

	## Loop through secondary objects and compare values

	for line in secondaries:

		# Load Keplarin elements for second object
		a_s  = float(line[1])
		e_s  = float(line[2])
		i_s  = float(line[3])
		W_s  = float(line[4])
		w_s  = float(line[5])

		# Convert secondary element angles from degrees to radians
		i_s = np.deg2rad(i_s)		# inclination
		W_s = np.deg2rad(W_s)		# RAAN
		w_s = np.deg2rad(w_s)		# argument of perigee

		# Compute deltas for primary and secodary object
		deltas  = compute_deltas(W_p = W_p, i_p = i_p, W_s = W_s, i_s = i_s)
		delta_p = deltas[0]
		delta_s = deltas[1]

		# Compute phase cosine for primary object and check if time filter can be applied
		cos_urs_p      = calc_cos_urs(a = a_p, e = e_p, w = w_p, delta = delta_p, D = D)
		cos_ur_p_plus  = cos_urs_p[0]
		cos_ur_p_minus = cos_urs_p[1]

		# Compute phase cosine for secondary object and check if time filter can be applied
		cos_urs_s      = calc_cos_urs(a = a_s, e = e_s, w = w_s, delta = delta_s, D = D)
		cos_ur_s_plus  = cos_urs_s[0]
		cos_ur_s_minus = cos_urs_s[1]

		# Compute time windows for primary object
		primary_windows = compute_time_window(cos_ur_plus = cos_ur_p_plus , cos_ur_minus = cos_ur_p_minus, w = w_p, a = a_p, e = e_p, delta = delta_p)		

		# Compute time windows for secondary object
		secondary_windows = compute_time_window(cos_ur_plus = cos_ur_s_plus , cos_ur_minus = cos_ur_s_minus, w = w_s, a = a_s, e = e_s, delta = delta_s)

		# Check if primary and secondary time windows overlap
		overlap_set = compare_time_windows(primary_windows = primary_windows, secondary_windows = secondary_windows)

		# add overlap sets to output directory
		overlap_sets.append(overlap_set)
		
	return overlap_sets

def investigate_cas_printer(jd_p, a_p, e_p, i_p, W_p, w_p, M_p, new_secondaries_3, overlap_sets, output_path):
	"""Function to print results of the investigation for later post processing"""

	with open(output_path, 'w') as fp:

		fp.write("# HERA close approach investigation\n")
		fp.write("# ==============================================================================================================\n")
		
		# convert primary object values to the appropriate format
		MJD  = "%1.15E" % jd_p
		SMA  = "%1.15E" % a_p
		ECC  = "%1.15E" % e_p
		INC  = "%1.15E" % i_p
		RAAN = "%1.15E" % W_p
		AOP  = "%1.15E" % w_p
		MA   = M_p

		# write orbels header
		header1 = 'MJD                  , SMA                  , ECC          '
		header2 = '        , INC                  , RAAN                 , AOP   '
		header3 = '               , MA                   , BSTAR                '
		fp.write("           {}{}{}\n".format(header1, header2, header3))

		# Define indexing for secondary orbels
		s_idx = 0

		for line in overlap_sets:

			# create indent
			fp.write(" \n")

			windows = line

			# define secondary orbels
			secondary_orbels = new_secondaries_3[s_idx]
		
			# write secondary orbels
			if len(secondary_orbels) >= 8:

				if len(secondary_orbels[7]) == 22:

					fp.write("Primary:   {}, {}, {}, {}, {}, {}, {}, \n".format(MJD, SMA, ECC, INC, RAAN, AOP, MA))
					fp.write("Secondary: {}, {}, {}, {}, {}, {}, {}, {}\n".format(secondary_orbels[0], secondary_orbels[1], secondary_orbels[2], secondary_orbels[3], secondary_orbels[4], secondary_orbels[5], secondary_orbels[6], secondary_orbels[7]))

				else:

					fp.write("Primary:   {}, {}, {}, {}, {}, {}, {}, \n".format(MJD, SMA, ECC, INC, RAAN, AOP, MA))
					fp.write("Secondary: {}, {}, {}, {}, {}, {}, {},  {}\n".format(secondary_orbels[0], secondary_orbels[1], secondary_orbels[2], secondary_orbels[3], secondary_orbels[4], secondary_orbels[5], secondary_orbels[6], secondary_orbels[7]))

			else:

				for idx, dummy in enumerate(secondary_orbels):

					fp.write("Primary:   {}, {}, {}, {}, {}, {}, {}, \n".format(MJD, SMA, ECC, INC, RAAN, AOP, MA))
					fp.write("Secondary: {}, ".format(secondary_orbels[idx]))

			for entry in windows:

				# define time windows
				prim_t_windows = entry[0]
				sec_t_windows  = entry[1]
				prim_f_windows = entry[2] 
				sec_f_windows  = entry[3]	

				# write windows
				fp.write(" \n")
				fp.write("primary t windows  : {}\n".format(prim_t_windows))
				fp.write("secondary t windows: {}\n".format(sec_t_windows))
				fp.write("primary f windows  : {}\n".format(prim_f_windows))
				fp.write("secondary f windows: {}\n".format(sec_f_windows))

			# update indexing
			s_idx = s_idx + 1

	return	

def investigateCasPrinterJson(jd_p, a_p, e_p, i_p, W_p, w_p, M_p, newSecondaries3, overlapSets, outputPath):
	"""Function to print details of prefilter-detected close approaches to json file"""

	# import packages
	import json

	# define output dictionary
	closeApproach = {
		"primary": {},
		"secondaries": {}
	}

	# add primary orbels to dictionary
	closeApproach["primary"] = {
		"MJD": "%1.15E" % jd_p,
		"SMA": "%1.15E" % a_p,
		"ECC": "%1.15E" % e_p,
		"INC": "%1.15E" % i_p,
		"RAAN": "%1.15E" % W_p,
		"AOP": "%1.15E" % w_p,
		"MA": M_p
	}

	# loop through overlapping secondary objects
	for idx, windows in enumerate(overlapSets, start = 0):

		# define secondary orbels
		secondaryOrbels = newSecondaries3[idx]

		# define time windows corresponding to secondary
		timeWindows = {}

		# populate
		for j, entry in enumerate(windows, start = 0):

			try:
				timeWindows["{}".format(j)] = {
					"prim_t_windows": entry[0],
					"sec_t_windows": entry[1],
					"prim_f_windows": entry[2], 
					"sec_f_windows": entry[3]
				}
			except IndexError:
				print("error caused by:")
				print(entry)
				print("------------")
				print(" ")	
				print("time windows:")
				for bitch in windows:
					print(bitch) 

		# add secondary data to dictionary
		closeApproach["secondaries"]["{}".format(idx)] = {
			"MJD": secondaryOrbels[0],
			"SMA": secondaryOrbels[1],
			"ECC": secondaryOrbels[2],
			"INC": secondaryOrbels[3],
			"RAAN": secondaryOrbels[4],
			"AOP": secondaryOrbels[5],
			"MA": secondaryOrbels[6],
			"BSTAR": secondaryOrbels[7],
			"timeWindows": timeWindows
		}
	
	# write dictionary to file
	with open(outputPath, "w") as fp:
		json.dump(closeApproach, fp, indent = 2)
