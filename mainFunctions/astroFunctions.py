def RVFromKepler(a, e, i, W, w, M, mu):

	# Import packages
	import numpy as np
	from mainFunctions.numericalFunctions import newtons_method

	## Convert mean anomaly to true anomaly
	
	# choose starting value for Newton's method
	if M  < np.pi:

		E_guess = M + e/2

	else:

		E_guess = M - e/2

	# define function and derivative for Newton's method (x = E)
	def f(x):
		return x - e*np.sin(x) - M;

	def df(x):
		return 1 - e*np.cos(x)		

	# run Newton's method
	output = newtons_method(f, df, E_guess, 1E-10, 100)

	E = output[0]

	# compute mean anomaly
	f = 2*np.arctan((((1+e)**(1/2))/((1-e)**(1/2)))*np.tan(E/2));

	## Perifocal State Vectors

	# Convert semi-major axis to angular momentum (km^2/s)
	h = (mu*a*(1 - e**2))**(1/2)

	# compute magnitude of position vector
	r = (h**2/mu) * (1/(1 + e*np.cos(f)))

	# Compute perifocal position vector
	R = r*np.array([[np.cos(f)], [np.sin(f)], [0]])

	# Compute perifocal velocity vector
	V = (mu/h)*np.array([[-np.sin(f)], [e + np.cos(f)], [0]])

	## Define Transformation Matrixes
	R3_W = np.array([[np.cos(W), np.sin(W), 0], [-np.sin(W), np.cos(W), 0], [0, 0, 1]])	# yaw & RA axis

	R1_i = np.array([[1, 0, 0], [0, np.cos(i), np.sin(i)], [0, -np.sin(i), np.cos(i)]])	# roll and inclinatio axis

	R3_w = np.array([[np.cos(w), np.sin(w), 0], [-np.sin(w), np.cos(w), 0], [0, 0, 1]])	# yaw and argument of perigee

	# Muliply transformations matrices to obtain transformation from perifocal to geocentric coordinate systems
	a = R3_w.dot(R1_i)
	Q_p_g = a.dot(R3_W)
	Q_p_g = Q_p_g.T

	# Apply transformation to obtain state vectors in the geocentric coordinate system
	V_ECI = Q_p_g.dot(V)
	R_ECI = Q_p_g.dot(R)

	# Compute magnitudes of position and velocity vectors
	r_mag = (R_ECI[0]**2 + R_ECI[1]**2 + R_ECI[2]**2)**(1/2)
	v_mag = (V_ECI[0]**2 + V_ECI[1]**2 + V_ECI[2]**2)**(1/2)

	return [R_ECI, V_ECI, r_mag, v_mag];

def position_mag_from_kep(a, e, i, W, w, M, mu):
	
	# Import packages
	import numpy as np
	from mainFunctions.numericalFunctions import dx, newtons_method

	## Convert mean anomaly to true anomaly
	
	# choose starting value for Newton's method
	if M  < np.pi:

		E_guess = M + e/2

	else:

		E_guess = M - e/2

	# define function and derivative for Newton's method (x = E)
	def f(x):
		return x - e*np.sin(x) - M;

	def df(x):
		return 1 - e*np.cos(x)		

	# run Newton's method
	output = newtons_method(f, df, E_guess, 1E-10, 100)

	E = output[0]

	# compute mean anomaly
	f = 2*np.arctan((((1+e)**(1/2))/((1-e)**(1/2)))*np.tan(E/2));

	## Perifocal State Vectors

	# Convert semi-major axis to angular momentum (km^2/s)
	h = (mu*a*(1 - e**2))**(1/2)

	# compute magnitude of position vector
	r = (h**2/mu) * (1/(1 + e*np.cos(f)))

	return r;

def position_mag_from_kep_with_f(a, e, i, W, w, f, mu):

	# import packages
	import numpy as np
	
	## Perifocal State Vectors

	# Convert semi-major axis to angular momentum (km^2/s)
	h = (mu*a*(1 - e**2))**(1/2)

	# compute magnitude of position vector
	r = (h**2/mu) * (1/(1 + e*np.cos(f)))

	return r;		

def E_and_f_from_M(M, e):

	# Import packages
	import numpy as np
	from mainFunctions.numericalFunctions import dx, newtons_method

	## Convert mean anomaly to true anomaly
	
	# choose starting value for Newton's method
	if M  < np.pi:

		E_guess = M + e/2

	else:

		E_guess = M - e/2

	# define function and derivative for Newton's method (x = E)
	def f(x):
		return x - e*np.sin(x) - M;

	def df(x):
		return 1 - e*np.cos(x)		

	# run Newton's method
	output = newtons_method(f, df, E_guess, 1E-10, 100)

	E = output[0]

	# compute mean anomaly
	f = 2*np.arctan((((1+e)**(1/2))/((1-e)**(1/2)))*np.tan(E/2));

	return [E, f];

def f_from_t(t, e, a, mu):

	# Import packages
	import numpy as np
	from mainFunctions.numericalFunctions import newtons_method

	## Convert mean anomaly to true anomaly

	# calculate  mean motion
	n = (mu/(a**3))**(1/2)

	# calculate mean anomaly
	M = n * t
	
	# choose starting value for Newton's method
	if M  < np.pi:

		E_guess = M + e/2

	else:

		E_guess = M - e/2

	# define function and derivative for Newton's method (x = E)
	def f(x):
		return x - e*np.sin(x) - M;

	def df(x):
		return 1 - e*np.cos(x)		

	# run Newton's method
	output = newtons_method(f, df, E_guess, 1E-10, 100)

	E = output[0]

	# compute mean anomaly
	f = 2*np.arctan((((1+e)**(1/2))/((1-e)**(1/2)))*np.tan(E/2));

	if f < 0:
		f = f + 2*np.pi

	return f ;	

def E(f, e):
	# import numpy
	import numpy as np

	erat = ((1 - e)/(1 + e))**(1/2)
	theta = (1/2)*f;
	return 2*np.arctan(erat*np.tan(theta));

def M(E, e):
	# import numpy
	import numpy as np

	return E - e*np.sin(E);		
