def processHeraOutput(inputDirectory, outputDirectory, configPath):

	### Import Packages
	import os
	import shutil
	import subprocess
	import numpy as np
	import matplotlib.pyplot as plt
	import json

	### Define Functions

	def cleanDir(dirPath):
		"""Function to create output directory and delete/recreate if it already exists"""

		# check if directory exists
		if os.path.isdir(dirPath) is True:

			# delete directory
			shutil.rmtree(dirPath)

			# replace directory
			os.makedirs(dirPath)

		else:
			
			# create directory
			os.makedirs(dirPath)	

		return 	

	def killDsStore(listIn):
		"""Function to kill all .DS_Store files in a list"""

		# define indexing
		idx = 0

		# loop through list and delete .DS_Store files
		for dummy in listIn:

			if listIn[idx].endswith(".DS_Store"):

				del listIn[idx]

				# step back indexing
				idx = idx - 1

			# update indexing
			idx = idx + 1

		return listIn

	def CreateListOfFiles(inputDir):
		"""Function to create a list of input files"""

		caInvestigateFiles = os.listdir("{}/close_approach_summaries".format(inputDir))

		# Remove DS.Store from list
		caInvestigateFiles = killDsStore(caInvestigateFiles)

		# Add primary designation to list
		for idx, line in enumerate(caInvestigateFiles):

			# split line at "_"
			splitString = line.split("_")

			# split line at "." i.e remove file extension
			splitString = splitString[-1].split(".")

			# create new entry
			newEntry = {
				"primDesig": splitString[0],
				"fullPath": "{}/close_approach_summaries/{}".format(inputDir, line)
			}

			# replace old entry with new entry
			caInvestigateFiles[idx] = newEntry	

		return caInvestigateFiles

	def createThalassaInputs(caInvestigateFiles, outputDirectory, inputDirectory, configPath):
		
		# load values from settings
		with open(configPath, 'r') as f_obj:
			settings = json.load(f_obj)			

		# clean output directory
		cleanDir(outputDirectory)

		# loop through close approach files
		for file in caInvestigateFiles:

			# open file and read in lines
			with open(file["fullPath"], 'r') as f_obj:

				inputs = json.load(f_obj)

			# create output directories
			fdir = "{}/ca_{}".format(outputDirectory, file["primDesig"])

			# clean directory
			cleanDir(fdir)

			# define output file
			fPathOut = "{}/objects.txt".format(fdir)

			# write output file
			with open(fPathOut, 'w') as fp:

				# write header
				fp.write("# Primary_{} Keplarian elements formatted for Thalassa\n".format(file["primDesig"]))
				fp.write("# ===========================================================================================================================================================================================================================================================================================\n")
				fp.write("MJD                      SMA                    ECC                    INC                    RAAN                   AOP                    MA                   Mass                   Area_drag              Area_SRP                    CD                     CR\n")

				# write primary orbels to file
				fp.write("{}, ".format(inputs["primary"]["MJD"]))
				fp.write("{}, ".format(inputs["primary"]["SMA"]))
				fp.write("{}, ".format(inputs["primary"]["ECC"]))
				fp.write("{}, ".format(inputs["primary"]["INC"]))
				fp.write("{}, ".format(inputs["primary"]["RAAN"]))
				fp.write("{}, ".format(inputs["primary"]["AOP"]))
				fp.write("{}, ".format(inputs["primary"]["MA"]))
				fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["primaryMass"]))
				fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["primaryAreaDrag"]))
				fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["primaryAreaSrp"]))
				fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["primaryCd"]))
				fp.write("{}\n".format("%1.15E" % settings["physicalChar"]["primaryCr"]))

				# write secondary orbels to file
				for dummy, value in inputs["secondaries"].items():

					# write secondary orbels to file
					fp.write("{}, ".format(value["MJD"]))
					fp.write("{}, ".format(value["SMA"]))
					fp.write("{}, ".format(value["ECC"]))
					fp.write("{}, ".format(value["INC"]))
					fp.write("{}, ".format(value["RAAN"]))
					fp.write("{}, ".format(value["AOP"]))
					fp.write("{}, ".format(value["MA"]))
					fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["secondaryMass"]))
					fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["secondaryAreaDrag"]))
					fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["secondaryAreaSrp"]))
					fp.write("{}, ".format("%1.15E" % settings["physicalChar"]["secondaryCd"]))
					fp.write("{}\n".format("%1.15E" % settings["physicalChar"]["secondaryCr"]))

		return
	
	# define additional functions
	def writeObject(orbelsLine, objectPath):

		# Simple function for Thalassa floating point formatting
		def numFormat(val_str):

			# convert to float
			val = float(val_str)
	
			if val < 0:

				out = "-{}".format(val_str)

			elif val >= 0:

				out = "+{}".format(val_str) 	

			return out 

		orbels = [x.strip() for x in orbelsLine.split(',')]

		# check for undefined values of MA
		if orbels[6] == "NaN":

			orbels[6] = "%1.15E" % 0

		MJD       = numFormat(orbels[0])
		SMA       = numFormat(orbels[1])
		ECC       = numFormat(orbels[2]) 
		INC       = numFormat(orbels[3]) 
		RAAN      = numFormat(orbels[4]) 
		AOP       = numFormat(orbels[5]) 
		MA        = numFormat(orbels[6]) 
		Mass      = numFormat(orbels[7]) 
		Area_drag = numFormat(orbels[8]) 
		Area_SRP  = numFormat(orbels[9]) 
		CD        = numFormat(orbels[10]) 
		CR        = numFormat(orbels[11])

		

		with open(objectPath, 'w') as fp:
			fp.write("# THALASSA - OBJECT PARAMETERS\n")
			fp.write("# ==============================================================================\n")
			fp.write("# Initial epoch and orbital elements\n")
			fp.write("{}; MJD  [UT1]\n".format(MJD))
			fp.write("{}; SMA  [km]\n".format(SMA))
			fp.write("{}; ECC  [-]\n".format(ECC))
			fp.write("{}; INC  [deg]\n".format(INC))
			fp.write("{}; RAAN [deg]\n".format(RAAN))
			fp.write("{}; AOP  [deg]\n".format(AOP))
			fp.write("{}; M    [deg]\n".format(MA))
			fp.write("# Physical characteristics\n")
			fp.write("{}; Mass [kg]\n".format(Mass))
			fp.write("{}; Area (drag) [m^2]\n".format(Area_drag))
			fp.write("{}; Area (SRP)  [m^2]\n".format(Area_SRP))
			fp.write("{}; CD   [-]\n".format(CD))
			fp.write("{}; CR   [-]\n".format(CR))

		return

	def writeSettings(settings, settingsPath, thalassaOutPath):

		# write file
		with open(settingsPath, 'w') as fp:

			fp.write("# THALASSA - SETTINGS\n")
			fp.write("# ==============================================================================\n")
			fp.write(" \n")
			fp.write("# PHYSICAL MODEL\n")
			fp.write("# insgrav:   0 = sph. grav. field, 1 = non-spherical grav. field.\n")
			fp.write("# isun:      0 = no Sun perturbation, 1 = otherwise.\n")
			fp.write("# imoon:     0 = no Moon perturbation, 1 = otherwise.\n")
			fp.write("# idrag:     0 = no atmospheric drag, 1 = Wertz model, 2 = US76 (PATRIUS), 3 = J77 (Carrara - INPE), 4 = NRLMSISE-00 (Picone - NRL)\n")
			fp.write("# iF107:     0 = constant F10.7 flux, 1 = variable F10.7 flux\n")
			fp.write("# iSRP:      0 = no SRP perturbation, 1 = SRP, no eclipses, 2 = SRP with conical Earth shadow\n")
			fp.write("# iephem:    Ephemerides source. 1 = DE431 ephemerides. 2 = Simpl. Meeus & Brown\n")
			fp.write("# gdeg:      Maximum degree of the gravitational potential.\n")
			fp.write("# gord:      Maximum order of the gravitational potential.\n")
			fp.write("insgrav:   {}\n".format(settings["physicalModel"]["insgrav"]))
			fp.write("isun:      {}\n".format(settings["physicalModel"]["isun"]))
			fp.write("imoon:     {}\n".format(settings["physicalModel"]["imoon"]))
			fp.write("idrag:     {}\n".format(settings["physicalModel"]["idrag"]))
			fp.write("iF107:     {}\n".format(settings["physicalModel"]["iF107"]))
			fp.write("iSRP:      {}\n".format(settings["physicalModel"]["iSRP"]))
			fp.write("iephem:    {}\n".format(settings["physicalModel"]["iephem"]))
			fp.write("gdeg:      {}\n".format(settings["physicalModel"]["gdeg"]))
			fp.write("gord:      {}\n".format(settings["physicalModel"]["gord"]))
			fp.write(" \n")
			fp.write("# INTEGRATION\n")
			fp.write("# tol:       Absolute = relative tolerance for the test propagation\n")
			fp.write("# tspan:     Propagation time span (solar days).\n")
			fp.write("# tstep:     Step size (solar days).\n")
			fp.write("# mxstep:    Maximum number of integration/output steps.\n")
			fp.write("tol:       {}\n".format(settings["integration"]["tol"]))
			fp.write("tspan:     {}\n".format(settings["integration"]["tspan"]))
			fp.write("tstep:     {}\n".format(settings["integration"]["tstep"]))
			fp.write("mxstep:    {}\n".format(settings["integration"]["mxstep"]))
			fp.write(" \n")
			fp.write("# EQUATIONS OF MOTION\n")
			fp.write("# eqs:       Type of the equations of motion.\n")
			fp.write("#            1 = Cowell, 2 = EDromo(t), 3 = EDromo(c), 4 = EDromo(l),\n")
			fp.write("#            5 = KS (t), 6 = KS (l), 7 = Sti-Sche (t), 8 = Sti-Sche (l)\n")
			fp.write("eqs:       {}\n".format(settings["eOfM"]["eqs"]))
			fp.write(" \n")
			fp.write("# OUTPUT SETTINGS\n")
			fp.write("# verb:      1 = Toggle verbose output, 0 = otherwise\n")
			fp.write("# out:       Full path to output directory\n")
			fp.write("verb:      {}\n".format(settings["output"]["verb"]))
			fp.write("out:   {}\n".format(thalassaOutPath))

		return

	def launchThalassa(thalassaDir, settingsPath, objectPath):

		arg1 = "./thalassa.x"
		arg2 = settingsPath
		arg3 = objectPath

		# change working directory to thalassa directory if not already there

		# check current working directory
		cwd = os.getcwd()

		if cwd == thalassaDir:

			# run thalassa
			subprocess.call([arg1, arg2, arg3], stdout = None)

		else:

			# change to thalassa direcotry	
			os.chdir(thalassaDir)

			# run thalassa
			subprocess.call([arg1, arg2, arg3], stdout = None)

		return

	def runThalassa(settings, outputDirectory, thalassaDir, intersectDistance):

		# define list of positve close approach cases
		positives = []

		# create list of folders

		try:
			directories = os.listdir(outputDirectory)

		except FileNotFoundError:

			return	

		# Remove DS.Store from list

		directories = killDsStore(directories)

		# make a settings file for each output direcotry
		for line in directories:

			# define list for checking if a case experiences close approaches
			caseCheck = []

			# define current primar object
			primObj = line

			# define objects path
			objectsPath = "{}/{}/objects.txt".format(outputDirectory, primObj)

			# define settings path
			settingsPath = "{}/{}/settings.txt".format(outputDirectory, primObj)

			# open objects file and grab primary and secondary orbels
			with open(objectsPath, 'r') as f_obj:

				# read lines
				lines = f_obj.readlines()

			# filter lines
			objects = lines[3:len(lines)]

			# sort objects
			primary     = objects[0]
			secondaries = objects[1:len(objects)]

			# create folders and input files for each object

			# primary

			# make input directory
			primaryDirIn = "{}/{}/primary/in/".format(outputDirectory, primObj)
			os.makedirs(primaryDirIn)

			# make output directory
			primaryDirOut = "{}/{}/primary/out/".format(outputDirectory, primObj)
			os.makedirs(primaryDirOut)

			# make input files
			objectPath   = "{}object.txt".format(primaryDirIn)
			settingsPath = "{}settings.txt".format(primaryDirIn)
			writeObject(primary, objectPath)
			writeSettings(settings, settingsPath, primaryDirOut)

			# launch thalassa
			launchThalassa(thalassaDir, settingsPath, objectPath)

			# define primary path
			primaryPath = "{}cart.dat".format(primaryDirOut)

			# secondaries
			for idx, dummy in enumerate(secondaries, start = 0):

				# define secondary object file designation
				secDesig = idx + 1

				# make input directory
				secondaryDirIn = "{}/{}/secondary_{}/in/".format(outputDirectory, primObj, secDesig)
				os.makedirs(secondaryDirIn)

				# make output directory
				secondaryDirOut = "{}/{}/secondary_{}/out/".format(outputDirectory, primObj, secDesig)
				os.makedirs(secondaryDirOut)

				# make input files
				objectPath   = "{}object.txt".format(secondaryDirIn)
				settingsPath = "{}settings.txt".format(secondaryDirIn)
				writeObject(secondaries[idx], objectPath)
				writeSettings(settings, settingsPath, secondaryDirOut)

				# launch thalassa
				launchThalassa(thalassaDir, settingsPath, objectPath)

				# define secondary path
				secondaryPath = "{}cart.dat".format(secondaryDirOut)

				# define path to save figure to
				figPath = "{}rSep.png".format(secondaryDirOut)

				# check for a close approach
				CaCheckOutput  = checkForCloseApproach(primaryPath, secondaryPath, intersectDistance, figPath)
				caCheck        = CaCheckOutput[0]
				minSepDistance = CaCheckOutput[1]

				if caCheck is True:

					# add primary and secodary path to positives list
					entry = [secondaryDirOut, minSepDistance]
					positives.append(entry)

					# add a 1 to caseCheck to indicate that there is a close approach associated with that primary
					caseCheck.append(1)

				elif caCheck is False:

					# delete secondary folder
					secondaryDir = "{}/{}/secondary_{}".format(outputDirectory, primObj, secDesig)
					shutil.rmtree(secondaryDir)
			
			# delete primary if case check is empty
			if not caseCheck:

				# delete close approach case directory
				caseDir = "{}/{}".format(outputDirectory, primObj)
				print("no close approches detected in case... deleting folder: {}".format(caseDir))
				shutil.rmtree(caseDir)

		return	positives

	def checkForCloseApproach(PrimaryObjPath, SecondaryObjPath, intersectDistance, figPath):

		# define object lists
		obj_1 = []
		obj_2 = []
		time  = []

		# define output
		r_sep = []

		# open first object and read in data
		with open(PrimaryObjPath, 'r') as f_obj:

			lines = f_obj.readlines()

			lines = lines[3:len(lines)]

		for line in lines:

			cart = [x.strip() for x in line.split(',')]

			MJD = cart[0]
			x   = cart[1]
			y   = cart[2]
			z   = cart[3]

			cart = [x, y, z]

			time.append(float(MJD))
			obj_1.append(cart)

		# open second ovject and read in dat
		with open(SecondaryObjPath, 'r') as f_obj:

			lines = f_obj.readlines()

			lines = lines[3:len(lines)]

		for line in lines:

			cart = [x.strip() for x in line.split(',')]

			x   = cart[1]
			y   = cart[2]
			z   = cart[3]

			cart = [x, y, z]

			obj_2.append(cart)

		# calculate relative separtion vector
		for idx, val in enumerate(time):	

			# primary
			try:
				
				r_1     = obj_1[idx]
				r_1_vec = np.array([[float(r_1[0]), float(r_1[1]), float(r_1[2])]])
			
			except IndexError:

				# print error message
				print("Indexing error in primary object...")
				print("path: {}".format(PrimaryObjPath))
				print("NaN value likely emerged in THALASSA state vector")
				
				# return false value for close approach
				caCheck = False

				if r_sep:

					return [caCheck, min(r_sep)]

				else:

					return [caCheck, "NaN"] 

			# secondary
			try:
				r_2     = obj_2[idx]
				r_2_vec = np.array([[float(r_2[0]), float(r_2[1]), float(r_2[2])]])

			except IndexError:

				# print error message
				print("Indexing error in secondary object...")
				print("path: {}".format(SecondaryObjPath))
				print("NaN value likely emerged in THALASSA state vector")
				
				# return false value for close approach
				caCheck = False

				if r_sep:

					return [caCheck, min(r_sep)]

				else:

					return [caCheck, "NaN"]

			# relative
			r_rel     = r_1_vec - r_2_vec
			r_rel_mag = (r_rel[0,0]**2 + r_rel[0,1]**2 + r_rel[0,2]**2)**(1/2)

			# add to list
			r_sep.append(r_rel_mag)

		# calculate minimum separation distance
		minSepDistance = min(r_sep)	

		# check if minimum value of r_sep is less than specified minimum
		if minSepDistance < intersectDistance:

			# print close approach notification
			print("Close approach within {} km detected".format(intersectDistance))
			print("location on disk: {}".format(SecondaryObjPath))
			
			# define check as true if list is not empty
			caCheck = True

			# plot results
			plt.plot(time, r_sep)
			plt.xlabel('modified julian date')
			plt.ylabel('relative separationd distance')
			plt.savefig(figPath)
			plt.close()	

		else:

			# define check as false if list is empty
			caCheck = False	

		return [caCheck, minSepDistance]	

	def processThalassaOutputs(outputDirectory, intersectDistance):

		# define list of positve close approach cases
		positives = []

		# create list of cases
		try:

			cases = os.listdir(outputDirectory)

		except FileNotFoundError:

			return positives	

		# Remove DS.Store from list

		cases = killDsStore(cases)
		
		# check for close approaches in each case
		for line in cases:

			caseDesig = line

			# define list for checking if a case experiences close approaches
			caseCheck = []

			# define primary object cartesian path:
			primaryPath = "{}/{}/primary/out/cart.dat".format(outputDirectory, caseDesig)

			# check for close approaches with secondary objects
			objects = os.listdir("{}/{}".format(outputDirectory, caseDesig))

			# Remove DS.Store from list

			objects = killDsStore(objects)

			# define a list for secondaries
			secondaries = []

			# grab secondaries
			for line in objects:
				
				# add to secondaries list of folder name begins with s
				if line[0] == "s":

					secondaries.append(line)

			# compare primaries to each secondary object
			for line in secondaries:

				# define secondary object designation
				secondaryDesig = line

				# define secondary object cartesian path
				secondaryPath = "{}/{}/{}/out/cart.dat".format(outputDirectory, caseDesig, secondaryDesig)

				# define path to save figure to
				figPath = "{}/{}/{}/rSep.png".format(outputDirectory, caseDesig, secondaryDesig)

				# check for a close approach
				CaCheckOutput  = checkForCloseApproach(primaryPath, secondaryPath, intersectDistance, figPath)
				caCheck        = CaCheckOutput[0]
				minSepDistance = CaCheckOutput[1]

				if caCheck is True:

					# define designation for secondary
					secDes = secondaryPath.split("/")
					ephem  = secDes[-5]
					obj    = secDes[-4]
					sec    = secDes[-3]
					secDes = "{}_{}_{}".format(ephem, obj, sec)

					# add primary and secodary path to positives list
					entry = [secDes, minSepDistance]
					positives.append(entry)

					# add a 1 to caseCheck to indicate that there is a close approach associated with that primary
					caseCheck.append(1)

				elif caCheck is False:

					# delete secondary folder
					shutil.rmtree("{}/{}/{}/".format(outputDirectory, caseDesig, secondaryDesig))
				
			# delete primary if case check is empty
			if not caseCheck:
				#print("deleting folder: {}/{}/".format(outputDirectory, caseDesig))
				shutil.rmtree("{}/{}/".format(outputDirectory, caseDesig))

		return positives

	### Main Algorithm (mainly for testing)

	# create a list of input files and primary object designations
	caInvestigateFiles = CreateListOfFiles(inputDirectory)

	# write Thalassa input files for each event
	createThalassaInputs(caInvestigateFiles, outputDirectory, inputDirectory, configPath)

	# load settings
	with open(configPath, 'r') as f_obj:
		settings = json.load(f_obj)

	# propagate objects
	positives = runThalassa(settings, outputDirectory, settings["general"]["thalassaDir"], settings["general"]["explicitIntersectDistance"])

	# check thalassa outputs for clsoe approaches
	#positives = processThalassaOutputs(outputDirectory, settings["general"]["explicitIntersectDistance"])

	return positives

#processHeraOutput(inputDirectory = "/Users/User/Documents/CODES/hera/data/heraEphemOutput/ephemeris_10", outputDirectory = "/Users/User/Desktop/caTest", configPath = "/Users/User/Documents/CODES/hera/data/postProcessing/postConfig.json")
