## Import Packages
import multiprocessing
import os
import sys
from joblib import Parallel, delayed
from postProcessFunctions.processCaSummaries import processHeraOutput

## Define Functions
def killDsStore(listIn):
	"""Function to kill all .DS_Store files in a list"""

	# define indexing
	idx = 0

	# loop through list and delete .DS_Store files
	for dummy in listIn:

		if listIn[idx].endswith(".DS_Store"):

			del listIn[idx]

			# step back indexing
			idx = idx - 1

		# update indexing
		idx = idx + 1

	return listIn	

def createListOfInputs(ephemDirec):
	"""Function to create a list of inputs based on the hera output ephemerides"""

	# create empty input list
	listOfInputs = []

	# create list of ephemerides
	listOfEphems = os.listdir(ephemDirec)

	# kill .DS_Store
	listOfEphems = killDsStore(listOfEphems)

	# create full input paths
	for idx, val in enumerate(listOfEphems, start = 0):

		# define path
		fullPath = "{}/{}".format(ephemDirec, listOfEphems[idx])

		# replace list entry
		listOfInputs.append(fullPath)

	return listOfInputs	

def createListOfOutputs(ephemDirec, outputDirectory):
	"""Create a list of output directories where the THALASSA propagation files will be stored"""
	
	# create empty output list
	listOfOutputs = []

	# create list of ephemerides
	listOfEphems = os.listdir(ephemDirec)

	# kill .DS_Store
	listOfEphems = killDsStore(listOfEphems)

	# create output directory if it doesnt' already exist
	if os.path.isdir(outputDirectory) == False:

		os.makedirs(outputDirectory)

	# create full output paths
	for idx, val in enumerate(listOfEphems):

		# define path
		fullPath = "{}/{}".format(outputDirectory, listOfEphems[idx])	

		# add directory 
		listOfOutputs.append(fullPath)

	return listOfOutputs

def createMultInputs(listOfInputs, listOfOutputs, settingsPath):
	"""Create list of inputs for mulitprocessing"""

	# create empty input list for multiprocessing
	INPUT = []

	# check that input and output lists are same length
	if len(listOfInputs) == len(listOfOutputs):

		# loop through lists and populate input list
		for idx, val in enumerate(listOfInputs, start = 0):

			# create entry
			entry = [listOfInputs[idx], listOfOutputs[idx], settingsPath]

			# add to input list
			INPUT.append(entry)

	else:

		# print error message
		print("ERROR:")
		print("input and output lists and unequal lenths...")

		# exit code
		sys.exit()

	return INPUT			

def MulitProcessHeraOutput(INPUT, numCores):
	"""Function to mulitprocess input list of mulitiple cpus"""

	# define function to process inputs
	def processInput(INPUT):
		"""Function to process the input"""

		# parse input
		inputDirectory  = INPUT[0]
		outputDirectory = INPUT[1]
		settingsPath    = INPUT[2]

		positives = processHeraOutput(inputDirectory, outputDirectory, settingsPath)

		return positives

	# define number of cores to be used
	if numCores == "all":

		numCores = multiprocessing.cpu_count()

	else:

		numCores = numCores

	# run process
	positives = Parallel(n_jobs = numCores)(delayed(processInput)(i) for i in INPUT)

	return [positives, numCores]

def writeSummary(closeApproaches, numCloseApproaches, outputDirectory):
	"""Function to write summary of HERA post processing"""

	# define summary path
	path = "{}/summary.txt".format(outputDirectory)

	# write summary
	with open(path, 'w') as fp:

		# write title
		fp.write("SUMMARY OF HERA POST PROCESSING WITH THALASSA\n")
		fp.write(" \n")
		fp.write("Total close approaches: {}\n".format(numCloseApproaches))
		fp.write(" \n")
		fp.write("Cases: \n")
		fp.write(" \n")

		for line in closeApproaches:

			# write to file
			fp.write("{}\n".format(line))

	return

def grabCloseApproaches(positives):
	"""Function to grab close approaches"""

	# define output list where close approaches may be placed
	closeApproachEphems = []

	# loop through each case(i.e. ephemeris input)
	for line in positives:

		# define each line as an ephemeris
		ephemeris = line

		# check if close approaches occured at ephemeris (i.e. if the list is not empty)
		if ephemeris:

			# add positive ephemeris to output list
			closeApproachEphems.append(ephemeris)

	return closeApproachEphems

def listCloseApproaches(closeApproachEphems):
	"""Function to define a list of close approaches"""

	# define a list of close approaches
	closeApproaches = [] 
	
	# loop through close approache ephemerides and count total number of close approaches for set
	for line in closeApproachEphems:

		# define ephemeris as line
		ephem = line

		for line in ephem:

			# add close approach to list
			closeApproaches.append(line)

	return closeApproaches	

def postProcess(settingsPath, ephemDirec, outputDirectory, numCores):
	"""main algorithm"""

	# create list of input paths
	listOfInputs = createListOfInputs(ephemDirec)

	# create list of output paths
	listOfOutputs = createListOfOutputs(ephemDirec, outputDirectory)

	# create multiprocessing inputs
	INPUT = createMultInputs(listOfInputs, listOfOutputs, settingsPath)

	# multiprocess HERA outputs
	output = MulitProcessHeraOutput(INPUT, numCores)

	# parse output
	positives = output[0]
	numCores  = output[1]

	# create list of ephemerides with close approaches
	closeApproachEphems = grabCloseApproaches(positives)

	# create a list of close approaches for set
	closeApproaches = listCloseApproaches(closeApproachEphems)

	# count number of clsoe approaches
	numCloseApproaches = len(closeApproaches)

	# write summary file
	writeSummary(closeApproaches, numCloseApproaches, outputDirectory)

	# print output
	print(" ")
	print("-----------------------------------------------------------------")
	print("{} cores utilized".format(numCores))
	print(" ")
	print("Total close approaches: {}".format(numCloseApproaches))
	print(" ")
	print(" Cases:")
	for line in closeApproaches:
		print(line)
	print("-----------------------------------------------------------------")	
	print(" ")

# # test
# sp = "/Users/User/Documents/CODES/hera/data/postProcessing/postconfig.json"
# ed = "/Users/User/Documents/CODES/hera/data/heraEphemOutput"
# od = "/Users/User/Documents/CODES/hera/data/postProcessing/results"
# postProcess(settingsPath = sp , ephemDirec = ed, outputDirectory = od, numCores = "all")
